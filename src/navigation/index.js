import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Splash from '../screens/Splash/Splash';
import LoginScreen from '../screens/Login/LoginScreen';
import RegistersScreen from '../screens/Register/RegistersScreen';
import ForgotScreen from '../screens/Register/ForgotScreen';
import WelcomeScreen from '../screens/WelcomeScreen/WelcomeScreen';
import PaymentCheck from '../screens/PaymentCheck';
import Payment from '../screens/Payment';
import Cart from '../screens/Cart';
import Checkout from '../screens/Checkout';
import ProfileScreen from '../screens/profil/ProfileScreen';
import EditProfilScreen from '../screens/editprofil/EditProfilScreen';
import GantiSIM from '../screens/gantiSIM/GantiSIM';
import StarterPackScreen from '../screens/starterPack/StarterPackScreen';
import DetilScreen from '../screens/Detil/DetilScreen';
import deviceBundlingScreen from '../screens/deviceBundling/deviceBundlingScreen';
import PascaBayarScreen from '../screens/pascaBayar/PascaBayarScreen';
import MsisdnScreen from '../screens/msisdn/MsisdnScreen';
import Address from '../screens/Address';
import Success from '../screens/Success';
import DetailHistory from '../screens/DetailHistory/DetailHistory';
import SearchScreen from '../screens/Search/SearchScreen';
import EditAddressScreen from '../screens/EditAddress/EditAddressScreen';
import AddressMapScreen from '../screens/AddressMap/AddressMapScreen';
import ChangePassword from '../screens/Register/ChangePassword';
import Homescreen from '../screens/homeScreen/Homescreen';
import VirtualPayment from '../screens/VirtualPayment/index';

import { navigationRef } from './RootNavigation';

const Stack = createStackNavigator();

const Navigation = () => {
  const [isLoadingMainScreen, setisLoadingMainScreen] = useState(true);
  const [tok , setTok] = useState(false);
  const cekToken = async () => {
    const item = await AsyncStorage.getItem('token');
    if (item){
      setTok(true);
    }
    else {
      setTok(false);
    }
  };

  useEffect(() => {
    cekToken();
  }, []);

  useEffect(()=>{
    setTimeout(async ()=>{
      setisLoadingMainScreen(false);
    }, 3500);
  });

  if (isLoadingMainScreen) {
    return <Splash />;
  }

  return (
    <>
      <NavigationContainer ref={navigationRef} >
        <Stack.Navigator headerMode="none" initialRouteName={WelcomeScreen} >
          {tok ?
            (
              <>
                <Stack.Screen name="Home" component={Homescreen} />
                <Stack.Screen name="WelcomeScreen" component={WelcomeScreen} />
              </>
            ) : (
              <>
                <Stack.Screen name="WelcomeScreen" component={WelcomeScreen} />
                <Stack.Screen name="Home" component={Homescreen} />
              </>
            )
          }
          <Stack.Screen name="Cart" component={Cart} />
          <Stack.Screen name="EditAddress" component={EditAddressScreen} />
          <Stack.Screen name="Registers" component={RegistersScreen} />
          <Stack.Screen name="Forgot" component={ForgotScreen} />
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Profil" component={ProfileScreen} />
          <Stack.Screen name="EditProfil" component={EditProfilScreen} />
          <Stack.Screen name="ChangePassword" component={ChangePassword} />
          <Stack.Screen name="Checkout" component={Checkout} />
          <Stack.Screen name="Payment" component={Payment} />
          <Stack.Screen name="PaymentCheck" component={PaymentCheck} />
          <Stack.Screen name="GantiSIM" component={GantiSIM} />
          <Stack.Screen name="StarterPack" component={StarterPackScreen} />
          <Stack.Screen name="Detil" component={DetilScreen} />
          <Stack.Screen name="Pascabayar" component={PascaBayarScreen} />
          <Stack.Screen name="Bundling" component={deviceBundlingScreen} />
          <Stack.Screen name="Msisdn" component={MsisdnScreen} />
          <Stack.Screen name ="Address" component={Address} />
          <Stack.Screen name ="AddressMap" component={AddressMapScreen} />
          <Stack.Screen name="Success" component={Success} />
          <Stack.Screen name="DetailHistory" component={DetailHistory} />
          <Stack.Screen name="Search" component={SearchScreen} />
          <Stack.Screen name="VirtualPayment" component={VirtualPayment} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default Navigation;
