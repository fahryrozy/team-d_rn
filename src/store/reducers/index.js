import { combineReducers } from 'redux';

import addressStoreReducer from './addressStoreReducer';
import orderStoreReducer from './orderStoreReducer';
import bankStoreReducer from './bankStoreReducer';
import profileStoreReducer from './profileStoreReducer';
import loginStoreReducer from './loginStoreReducer';
import productStoreReducer from './productStoreReducer';
import forgotStoreReducer from './forgotStoreReducer';
import reviewStoreReducer from './reviewStoreReducer';
import transHisStoreReducer from './transHisStoreReducer';
import paymentStoreReducer from './paymentStoreReducer';

const reducers = {
  addressStore: addressStoreReducer,
  orderStore: orderStoreReducer,
  bankStore: bankStoreReducer,
  profileStore: profileStoreReducer,
  loginStore: loginStoreReducer,
  productStore: productStoreReducer,
  forgotStore: forgotStoreReducer,
  reviewStore: reviewStoreReducer,
  transHisStore: transHisStoreReducer,
  paymentStore: paymentStoreReducer
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
