const initialState = {
  isLoading: false,
  idTransaction: null,
  vaNumber: null,
  bank: null,
  statusMessage: null
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'VIRTUAL_PAYMENT_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'VIRTUAL_PAYMENT_SUCCESS': {
      return {
        ...state,
        bank: action.payload.bank,
        idTransaction: action.payload.idTransaction,
        vaNumber: action.payload.vaNumber,
        isLoading: false
      };
    }
    case 'VIRTUAL_PAYMENT_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'CONFIRM_VIRTUAL_PAYMENT_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'CONFIRM_VIRTUAL_PAYMENT_SUCCESS': {
      return {
        ...state,
        bank: null,
        id_transaction: null,
        vaNumber: null,
        isLoading: false
      };
    }
    case 'CONFIRM_VIRTUAL_PAYMENT_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    default:
      return state;
  }
};

