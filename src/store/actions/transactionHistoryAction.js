import { getHistory } from '../../api/transaction';

export const getTransactionHistory = () => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_TRANSACTION_HISTORY_REQUEST'
    });

    try {
      const result = await getHistory();
      if (result.status === 200) {
        dispatch({
          type: 'GET_TRANSACTION_HISTORY_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'GET_TRANSACTION_HISTORY_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_TRANSACTION_HISTORY_FAILED',
        error: err
      });
    }
  };
};
