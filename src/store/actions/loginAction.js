import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert } from 'react-native';

import * as RootNavigation from '../../navigation/RootNavigation';
import { login, refresh_token } from '../../api/login';

export const goLogin = (email, password) => {
  const storeToken = async (val) => {
    try {
      await AsyncStorage.setItem('token', val);
      return true;
    } catch (e) {
      Alert.alert('Store token eror');
    }
  };
  const refreshToken = async (val) => {
    try {
      await AsyncStorage.setItem('refresh_token', val);
      RootNavigation.reset('Home');
      return true;
    } catch (e) {
      Alert.alert('Store refresh token error!');
    }
  };
  return async (dispatch) => {
    dispatch({
      type: 'LOGIN_REQUEST'
    });

    try {
      const body = { email, password };
      const result = await login(body);
      if (result.status === 201) {
        dispatch({
          type: 'LOGIN_SUCCESS',
          payload: result.data.result
        });
        storeToken(result.data.result.token);
        refreshToken(result.data.result.refresh_token);
      }
      else {
        dispatch({
          type: 'LOGIN_FAILED',
          error: result.data
        });
        if (result === 'Username or password invalid'){
          Alert.alert('Username atau password salah');
        }
        else {
          Alert.alert('Akun belum aktif, silahkan aktivasi melalui email');}
      }
    } catch (error) {
      dispatch({
        type: 'LOGIN_FAILED',
        error: error
      });
    }
  };
};

export const goRefresh = (refresh) => {
  const storeToken = async (val) => {
    try {
      await AsyncStorage.setItem('token', val);
      return true;
    } catch (e) {
      Alert.alert('Store token eror');
    }
  };

  const refreshToken = async (val) => {
    try {
      await AsyncStorage.setItem('refresh_token', val);
      return true;
    } catch (e) {
      Alert.alert('Store refresh token error!');
    }
  };

  return async (dispatch) => {
    dispatch({
      type: 'REFRESH_REQUEST'
    });

    try {
      const body = { refresh };
      const result = await refresh_token(body);
      if (result.status === 201) {
        dispatch({
          type: 'REFRESH_SUCCESS',
          payload: result.data.result
        });
        storeToken(result.data.result.token);
        refreshToken(result.data.result.refresh_token);
      }
      else {
        dispatch({
          type: 'REFRESH_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'REFRESH_FAILED',
        error: err
      });
    }
  };
};
