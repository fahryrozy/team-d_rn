import { ToastAndroid } from 'react-native';

import { getDataListBank } from '../../api/payment';

export const getListBank = () => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_LIST_BANK_REQUEST'
    });

    try {
      const result = await getDataListBank();
      if (result.status === 200) {
        dispatch({
          type: 'GET_LIST_BANK_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'GET_LIST_BANK_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_LIST_BANK_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const selectBank = (item) => {
  if (item.isVirtualAccount) {
    return async (dispatch) => {
      dispatch({
        type: 'SELECT_BANK',
        payload: item.id,
        isPayWithVirtualBank: true,
        virtualBank: item.bank
      });
    };
  }
  else {
    return async (dispatch) => {
      dispatch({
        type: 'SELECT_BANK',
        payload: item.id,
        isPayWithVirtualBank: false,
        virtualBank: null
      });
    };
  }
};
