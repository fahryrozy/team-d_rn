
import { ToastAndroid } from 'react-native';

import { getDataOrders, addItemQuantity, subsItemQuantity, getDataShipmentPrice } from './../../api/transaction';

export const getOrders = () => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_LIST_ORDER_REQUEST'
    });

    try {
      const result = await getDataOrders();
      if (result.status === 200) {
        dispatch({
          type: 'GET_LIST_ORDER_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'GET_LIST_ORDER_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_LIST_ORDER_FAILED',
        error: err
      });
    }
  };
};

export const addQuantity = (product_id, qty) => {
  return async (dispatch) => {
    dispatch({
      type: 'ADD_QUANTITY_ORDER_REQUEST'
    });

    try {
      const result = await addItemQuantity(product_id, qty);
      if (result.status === 201) {
        dispatch({
          type: 'ADD_QUANTITY_ORDER_SUCCESS',
          product_id,
          qty,
          payload: result.data
        });
      } else {
        dispatch({
          type: 'ADD_QUANTITY_ORDER_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'ADD_QUANTITY_ORDER_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const subsQuantity = (product_id, qty, number) => {
  return async (dispatch) => {
    dispatch({
      type: 'SUBS_QUANTITY_ORDER_REQUEST'
    });

    try {
      const result = await subsItemQuantity(product_id, qty, number);
      if (result.status === 201) {
        dispatch({
          type: 'SUBS_QUANTITY_ORDER_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'SUBS_QUANTITY_ORDER_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'SUBS_QUANTITY_ORDER_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const delQuantity = (product_id, number) => {
  return async (dispatch) => {
    dispatch({
      type: 'DEL_QUANTITY_ORDER_REQUEST'
    });

    try {
      const result = await subsItemQuantity(product_id, 0, number);
      if (result.status === 201) {
        dispatch({
          type: 'DEL_QUANTITY_ORDER_SUCCESS',
          payload: result.data,
          product_id
        });
      } else {
        dispatch({
          type: 'DEL_QUANTITY_ORDER_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'DEL_QUANTITY_ORDER_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const processedFromHistory = (id_transaction, totalPrice) => {
  return async (dispatch) => {
    dispatch({
      type: 'PROCESSED_FROM_HISTORY',
      id_transaction,
      totalPrice
    });
  };
};

export const getShipmentPrice = (addressId, totalWeight) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_SHIPMENT_PRICE_REQUEST'
    });

    try {
      const body = { addressId, totalWeight };
      const result = await getDataShipmentPrice(body);
      if (result.status === 200) {
        dispatch({
          type: 'GET_SHIPMENT_PRICE_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'GET_SHIPMENT_PRICE_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_SHIPMENT_PRICE_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};
