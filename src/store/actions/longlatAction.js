import * as RootNavigation from '../../navigation/RootNavigation';

export const setLongLat = (long, lat) => {
  const body = { long, lat };
  return async (dispatch) => {
    dispatch({
      type: 'SET_LONG_LAT',
      payload: body
    });
    RootNavigation.pop();
  };
};

export const myLocation = (long, lat) => {
  const body = { long, lat };
  return async (dispatch) => {
    dispatch({
      type: 'SET_MY_LOCATION',
      payload: body
    });
    RootNavigation.navigate('AddressMap');
  };
};
