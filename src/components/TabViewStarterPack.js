import * as React from 'react';
import { useState, useEffect } from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
  Dimensions
} from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { useSelector, useDispatch } from 'react-redux';
import SwitchSelector from 'react-native-switch-selector';

import { getDataProduct } from '../store/actions/productAction';

import ProductCard from './ProductCard';
import ProductCardList from './ProductCardList';

const renderTabBar = (props) => (
  <TabBar
    {...props}
    indicatorStyle={{ backgroundColor: 'white' }}
    style={{ backgroundColor: '#b51320' }}
  />
);

const FirstRoute = (data) => (
  <>
    <ScrollView
      style={{ backgroundColor: '#f0f0f0' }}
      showsVerticalScrollIndicator={false}>
      <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
        {data.toogle === true ? (
          <ProductCardList subProduct={'Simpati'} product={data} />
        ) : (
          <ProductCard subProduct={'Simpati'} product={data} />
        )}
      </View>
    </ScrollView>
  </>
);

const SecondRoute = (data) => (
  <ScrollView
    style={{ backgroundColor: '#f0f0f0' }}
    showsVerticalScrollIndicator={false}>
    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
      {data.toogle === true ? (
        <ProductCardList subProduct={'KartuAs'} product={data} />
      ) : (
        <ProductCard subProduct={'KartuAs'} product={data} />
      )}
    </View>
  </ScrollView>
);

const ThirdRoute = (data) => (
  <ScrollView
    style={{ backgroundColor: '#f0f0f0' }}
    showsVerticalScrollIndicator={false}>
    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
      {data.toogle === true ? (
        <ProductCardList subProduct={'Loop'} product={data} />
      ) : (
        <ProductCard subProduct={'Loop'} product={data} />
      )}
    </View>
  </ScrollView>
);

const TabViewExample = () => {
  const [isEnabled, setIsEnabled] = useState(false);
  const tab = useSelector((state) => state.productStore.tab);
  const dispatch = useDispatch();
  const product = useSelector((state) => state.productStore.product);
  const [index, setIndex] = useState(tab);
  const [routes] = useState([
    { key: 'first', title: 'Simpati' },
    { key: 'second', title: 'Kartu As' },
    { key: 'third', title: 'Loop' }
  ]);

  const _handleIndexChange = (_index) => setIndex(_index);

  useEffect(() => {
    dispatch(getDataProduct());
  }, []);

  return (
    <>
      <TabView
        renderTabBar={renderTabBar}
        navigationState={{ index, routes }}
        renderScene={SceneMap({
          first: () => <FirstRoute data={product} toogle={isEnabled.value} />,
          second: () => <SecondRoute data={product} toogle={isEnabled.value} />,
          third: () => <ThirdRoute data={product} toogle={isEnabled.value} />
        })}
        onIndexChange={_handleIndexChange}
        initialLayout={{ width: Dimensions.get('window').width }}
        style={styles.container}
      />
      <SwitchSelector
        initial={0}
        onPress={(value) => setIsEnabled({ value })}
        buttonColor ={'#b51320'}
        hasPadding
        options={[
          { label: 'Grid View', value: false },
          { label: 'List View', value: true }
        ]}
      />
    </>
  );
};

export default TabViewExample;

const styles = StyleSheet.create({
  container: {
    marginTop: 0
  },
  scene: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
