import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import { useSelector } from 'react-redux';

const { width } = Dimensions.get('window');

const Bank = ({ data, onPress }) => {
  const image = { uri: `${data.logo}` };
  const selectedBank = useSelector((state) => state.bankStore.selectedBank);
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={selectedBank === data.id ? styles.cardContainerActive : styles.cardContainer}>
        <View style={styles.imageContainer}>
          {data.logo && <Image source={image} style={styles.logo} />}
        </View>
        <View style={styles.textContainer}>
          {data.bank && <Text style={styles.labelName}>{data.bank.toUpperCase()}</Text>}
          {data.account_number === 'virtual account number' ?
            <View>
              <Text style={styles.labelAccount}>Virtual Account</Text>
              <Text style={styles.labelMethod}>Pengecekan Otomatis</Text>
            </View>
            : <View>
              <Text style={styles.labelAccount}>{data.account_number} </Text>
              <Text style={styles.labelMethod}>Pengecekan Manual</Text>
            </View>
          }
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Bank;

const styles = StyleSheet.create({
  cardContainer: {
    flexDirection: 'row',
    backgroundColor: '#FFF',
    borderRadius: 20,
    paddingVertical: 15,
    paddingHorizontal: 20,
    alignItems: 'center',
    marginVertical: 5
  },
  cardContainerActive: {
    flexDirection: 'row',
    backgroundColor: '#FFF',
    borderRadius: 20,
    paddingVertical: 15,
    paddingHorizontal: 20,
    alignItems: 'center',
    marginVertical: 5,
    borderWidth: 1,
    borderColor: '#F18F40'
  },
  imageContainer: {
    paddingHorizontal: 10
  },
  logo: {
    width: 75,
    height: 75,
    resizeMode: 'contain'
  },
  textContainer: {
    paddingHorizontal: 10
  },
  labelName: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  labelAccount: {
    fontSize: 16
  },
  labelMethod: {
    fontSize: width * 0.03,
    color: '#F18F40',
    fontWeight: 'bold'
  }

});
