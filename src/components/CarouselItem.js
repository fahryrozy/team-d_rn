import React from 'react';
import { View, StyleSheet, Text, Image, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const CarouselItem = ({ item }) => {
  return (
    <View >
      <View style={styles.pictureView}>
        <Image style={styles.image} source={item.url} />
        <Text style={styles.itemTitle}> {item.title}</Text>
        <Text style={styles.itemDescription}>{item.description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pictureView: {
    position: 'absolute',
    width: width - 20,
    height: width
  },
  textView: {
    flex: 1,
    marginTop: height * 0.39,
    position: 'absolute',
    left: 5,
    alignItems: 'center'
  },
  image: {
    flex: 1,
    width: width - 20,
    height: height / 2,
    borderRadius: 10
  },
  itemTitle: {
    color: 'white',
    fontSize: 28,
    shadowColor: '#000',
    shadowOffset: { width: 0.8, height: 0.8 },
    shadowOpacity: 1,
    shadowRadius: 3,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingHorizontal: width * 0.15,
    width: width
  },
  itemDescription: {
    color: 'white',
    fontSize: 12,
    shadowColor: '#000',
    shadowOffset: { width: 0.8, height: 0.8 },
    shadowOpacity: 1,
    shadowRadius: 3,
    alignItems: 'center',
    width: width,
    paddingHorizontal: width * 0.08
  }
});

export default CarouselItem;
