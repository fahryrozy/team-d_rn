import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import NumberFormat from 'react-number-format';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch } from 'react-redux';

import { addQuantity, subsQuantity, delQuantity } from '../store/actions/orderAction';

const OrderList = ({ data }) => {
  const image = { uri: `${data.poster}` };
  const dispatch = useDispatch();

  const addHandler = () => {
    dispatch(addQuantity(data.product_id, data.qty + 1));
  };

  const subsHandler = () => {
    dispatch(subsQuantity(data.product_id, data.qty - 1, {}));
  };

  const deleteHandler = () => {
    dispatch(delQuantity(data.product_id, data.number));
  };

  return (
    <View style={styles.cardContainer}>
      <View style={styles.imageContainer}>
        {data.poster ? <Image source={image} style={styles.logo} /> : <Image source={require('../asset/image/simcard.png')} style={styles.logo} />}
      </View>
      <View style={styles.textContainer}>

        <View style={styles.actionContainer}>
          {data.productCategoryId === 4 ?
            (
              <>
                {data.qty > 1 ?
                  (
                    <TouchableOpacity style={styles.deleteButton} onPress={subsHandler}>
                      <MaterialCommunityIcons name="minus-circle-outline" size={15} color={'#FFF'} />
                    </TouchableOpacity>
                  )
                  : (
                    <TouchableOpacity style={styles.deleteButton} onPress={deleteHandler}>
                      <Ionicons name="trash-sharp" size={14} color={'#FFF'} />
                    </TouchableOpacity>
                  )
                }
                <View style={{ backgroundColor: '#FFF', elevation: 1, paddingHorizontal: 5, paddingVertical: 3, opacity: 0.7 }}>
                  <Text>{data.qty}</Text>
                </View>
                <TouchableOpacity style={styles.addButton} onPress={addHandler}>
                  <Ionicons name="add-circle-outline" size={16} color={'#FFF'} />
                </TouchableOpacity>
              </>
            )
            : (

              <TouchableOpacity style={styles.trashButton} onPress={deleteHandler}>
                <Ionicons name="trash-sharp" size={15} color={'#FFF'} />
              </TouchableOpacity>
            )
          }
        </View>
        <View style={styles.productDescription}>
          {data && <Text style={styles.labelCategory}>{data.name}</Text>}
          {data && <Text style={styles.labelName} multiline={true} numberOfLines={2} >{data.number}</Text>}
          <View style={styles.priceContainer}>
            {data.price && data.price > 0 ?
              (
                <NumberFormat value={data.price}
                  displayType={'text'} prefix={'Rp. '}
                  renderText={value => <Text style={styles.labelPrice}>{value}</Text>}
                  thousandSeparator={'.'} decimalSeparator={','}
                />
              )
              : (
                <Text style={styles.labelPrice}>Free</Text>
              )
            }
          </View>
        </View>
      </View>
    </View>
  );
};

export default OrderList;

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#FFF',
    borderRadius: 20,
    paddingVertical: 20,
    paddingHorizontal: 5,
    alignItems: 'center',
    marginVertical: 5
  },
  imageContainer: {
    flex: 1,
    paddingHorizontal: 5
  },
  logo: {
    width: 100,
    height: 100,
    resizeMode: 'contain'
  },
  textContainer: {
    flex: 2,
    paddingLeft: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  labelCategory: {
    color: '#F18F40',
    fontWeight: '800'
  },
  labelName: {
    fontSize: 18,
    marginVertical: 10,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  priceContainer: {
    backgroundColor: '#de3030',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 100,
    marginVertical: 5,
    alignItems: 'center'
  },
  labelPrice: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFF'
  },
  actionContainer: {
    flexDirection: 'row',
    paddingLeft: 10,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    marginTop: -15,
    marginBottom: 15
  },
  deleteButton: {
    padding: 5,
    backgroundColor: '#CD413A',
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50
  },
  trashButton: {
    padding: 5,
    backgroundColor: '#CD413A',
    borderRadius: 50
  },
  addButton: {
    padding: 4,
    backgroundColor: '#41CD3A',
    borderTopRightRadius: 50,
    borderBottomRightRadius: 50
  }
});
