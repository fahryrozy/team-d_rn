import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const ButtonSheet = ({ title, subtitle, style, titleStyle, ...others }) => (
  <View style={[styles.panelButton, style]}>
    <TouchableOpacity style={styles.button} {...others}>
      {subtitle && (
        <Text style={styles.buttonSubText}>{subtitle}</Text>
      )
      }
      <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
        <Text style={[styles.buttonText, titleStyle]}>{title}</Text>
        <Ionicons name="chevron-forward-circle" size={30} color={'#DE3030'} style={{ paddingHorizontal: 10, opacity: 0.3 }} />
      </View>
    </TouchableOpacity>
  </View>
);

export default ButtonSheet;

const styles = StyleSheet.create({
  panelButton: {
    paddingVertical: 10,
    width: '100%',
    alignItems: 'center'
  },
  button: {
    borderColor: '#DE3030',
    paddingVertical: 10,
    borderWidth: 1,
    paddingHorizontal: 10,
    borderRadius: 100,
    justifyContent: 'center',
    backgroundColor: '#FFF',
    marginVertical: 5
  },
  buttonText: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#DE3030',
    paddingHorizontal: 20
  },
  buttonSubText: {
    fontSize: 10,
    fontWeight: 'bold',
    color: '#DE3030',
    paddingHorizontal: 20
  }
});
