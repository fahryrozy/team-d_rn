import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Config from 'react-native-config';
import { ToastAndroid } from 'react-native';

import * as RootNavigation from '../navigation/RootNavigation';

export const fetchAPIProduction = async (method, path, body) => {
  try {
    const token = await AsyncStorage.getItem('token');
    const req = await axios({
      method: method,
      url: `${Config.BASE_URL}${path}`,
      data: body,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    });
    if (req.status !== 400) {
      return req;
    }
  } catch (e) {
    if (e.response.status === 401) {
      try {
        const token = await AsyncStorage.getItem('token');
        const refreshToken = await AsyncStorage.getItem('refresh_token');
        const refreshTokenBody = { refresh_token: refreshToken };

        const refreshTokenRequest = await axios({
          method: 'POST',
          url: `${Config.BASE_URL}auth/refresh`,
          data: refreshTokenBody,
          headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
          }
        });
        if (refreshTokenRequest.status === 201) {
          const newToken = refreshTokenRequest.data.result.token;
          const newRefreshToken = refreshTokenRequest.data.result.refresh_token;
          await AsyncStorage.setItem('token', newToken);
          await AsyncStorage.setItem('refresh_token', newRefreshToken);
        }
      } catch (error) {
        throw error;
      }
    }
    throw e;
  }
};

export const fetchChangePassword = async (method,path, body) =>{
  const token = await AsyncStorage.getItem('forgot_token');
  try {
    const result = await axios({
      method: method,
      url: `${Config.BASE_URL}${path}`,
      data: body,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Apikey 13d7def7623f454ccbf93453870e97f1 Bearer ${token}`
      }
    });
    return result;
  } catch (e){
    throw e;
  }
};

export const fetchReviewAPI = async (method, path, body) => {
  try {
    const token = await AsyncStorage.getItem('token');
    const req = await axios({
      method: method,
      url: `${Config.BASE_URL}${path}`,
      params: body,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    });
    if (req.status !== 400) {
      return req;
    }
  } catch (e) {
    if (e.response.status === 401) {
      try {
        const token = await AsyncStorage.getItem('token');
        const refreshToken = await AsyncStorage.getItem('refresh_token');
        const refreshTokenBody = { refresh_token: refreshToken };

        const refreshTokenRequest = await axios({
          method: 'POST',
          url: `${Config.BASE_URL}auth/refresh`,
          data: refreshTokenBody,
          headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
          }
        });
        if (refreshTokenRequest.status === 201) {
          const newToken = refreshTokenRequest.data.result.token;
          const newRefreshToken = refreshTokenRequest.data.result.refresh_token;
          await AsyncStorage.setItem('token', newToken);
          await AsyncStorage.setItem('refresh_token', newRefreshToken);
        }
      } catch (error) {
        throw error;
      }
    }
    throw e;
  }
};

export const fetchAPIWilayahIndonesia = async (method, path) => {
  try {
    const req = await axios({
      method: method,
      url: `${Config.DATA_WILAYAH_INDONESIA}${path}`,
      headers: {
        'Authorization': `${Config.RUANG_API_KEY}`
      }
    });
    if (req.status !== 400) {
      return req;
    }
  } catch (e) {
    throw e;
  }
};

export const refreshTokenHandler = async () => {
  try {
    const token = await AsyncStorage.getItem('token');
    const refreshToken = await AsyncStorage.getItem('refresh_token');
    const refreshTokenBody = { refresh_token: refreshToken };

    const refreshTokenRequest = await axios({
      method: 'POST',
      url: `${Config.BASE_URL}auth/refresh`,
      data: refreshTokenBody,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    });
    if (refreshTokenRequest.status === 201) {
      const newToken = refreshTokenRequest.data.result.token;
      const newRefreshToken = refreshTokenRequest.data.result.refresh_token;
      await AsyncStorage.setItem('token', newToken);
      await AsyncStorage.setItem('refresh_token', newRefreshToken);
    }
  } catch (error) {
    ToastAndroid.showWithGravity('Sesi anda telah berakhir, silahkan login kembali', ToastAndroid.LONG, ToastAndroid.BOTTOM);
    await AsyncStorage.clear();
    RootNavigation.reset('Login');
    throw (error);
  }
};

export const getAppVersion = async () => {
  try {
    const req = await axios({
      method: 'GET',
      url: `${Config.BASE_URL}update/latest`
    });
    if (req.status === 200) {
      // eslint-disable-next-line prefer-destructuring
      const { version } = req.data.result[0];
      return version;
    }
  } catch (e) {
    throw e;
  }
};
