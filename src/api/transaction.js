import { fetchAPIProduction } from '../utils/fetchAPI';

export const getDataOrders = async () => {
  try {
    const result = await fetchAPIProduction('GET', 'transaction/cart');
    return result;
  } catch (error) {
    throw error;
  }
};

export const checkoutDataOrder = async (transaction, address) => {
  try {
    const result = await fetchAPIProduction('GET', `transaction/checkout?id_transaction=${transaction}&address_id=${address}`);
    return result;
  } catch (error) {
    throw error;
  }
};

export const addItemQuantity = async (product_id, qty) => {
  try {
    const result = await fetchAPIProduction('PATCH', 'transaction/cart', { product_id, qty });
    return result;
  } catch (error) {
    throw error;
  }
};

export const subsItemQuantity = async (product_id, qty, number) => {
  try {
    const result = await fetchAPIProduction('PATCH', 'transaction/cart', { product_id, qty, number });
    return result;
  } catch (error) {
    throw error;
  }
};

export const getHistory = async () => {
  try {
    const result = await fetchAPIProduction('GET', 'transaction/history');
    return result;
  } catch (error) {
    throw error;
  }
};

export const getDataShipmentPrice = async (body) => {
  try {
    const result = await fetchAPIProduction('POST', 'shipping/costsExisting', body);
    return result;
  } catch (error) {
    throw error;
  }
};
