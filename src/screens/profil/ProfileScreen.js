import React, { useState, useEffect } from 'react';
import { View, Text, Image, Dimensions, ImageBackground, TouchableOpacity, FlatList, Alert } from 'react-native';
const { width, height } = Dimensions.get('window');
import AsyncStorage from '@react-native-async-storage/async-storage';
import Feather from 'react-native-vector-icons/Feather';
import BottomSheet from 'reanimated-bottom-sheet';
import SwitchSelector from 'react-native-switch-selector';
import LinearGradient from 'react-native-linear-gradient';

import NumberFormat from 'react-number-format';
import { useSelector, useDispatch } from 'react-redux';

import moment from 'moment';
import 'moment/locale/id';

import { getDataProfile } from '../../store/actions/profileAction';
import { processedFromHistory } from '../../store/actions/orderAction';
import { selectBank } from '../../store/actions/bankAction';
import { getTransactionHistory } from '../../store/actions/transactionHistoryAction';

import styles from './ProfileStyles';



const ProfileScreen = ({ navigation }) => {
  const profile = useSelector((state) => state.profileStore.profile);
  const dataHistory = useSelector((state) => state.transHisStore.dataTransaction);

  const dispatch = useDispatch();
  const [showProfile, setShowProfile] = useState(true);
  const [data, setData] = React.useState({
    email: '',
    isLoading: false,
    onError: false,
    onMessage: null,
    name: '',
    avatar: null,
    id: '',
    createdAt: null,
    updatedAt: null,
    token: null,
    change: false
  });
  const isUpdatingProfile = useSelector((state) => state.profileStore.isUpdatingProfile);
  useEffect(() => {
    dispatch(getDataProfile());
  }, [isUpdatingProfile]
  );


  const renderItem = ({ item }) => {
    moment.locale();
    let productAmount = item.products.length;
    let productOthers = productAmount - 1;
    const handleProcess = async (id_transaction, totalPrice, shippingCost) => {
      dispatch(processedFromHistory(id_transaction, totalPrice + shippingCost));
      navigation.navigate('Payment');
    };
    const handlePending = async (id_transaction, totalPrice, paymentMethod, isVirtualAccount, bank, virtualAccount) => {
      if (paymentMethod !== null){
        if (isVirtualAccount === true) {
          Alert.alert('Peringatan', `Silahkan selesaikan pembayaran otomatis anda pada virtual account ${bank} : ${virtualAccount}`);
        } else {
          dispatch(processedFromHistory(id_transaction, totalPrice));
          dispatch(selectBank(id_transaction, totalPrice, paymentMethod));
          navigation.navigate('PaymentCheck');
        }
      } else {
        Alert.alert('Peringatan', 'Silahkan selesaikan pembayaran');

      }
    };
    return (
      <TouchableOpacity style={styles.containerTransHis}
        onPress={()=>
          item.status === 'PROCESS' ? (
            handleProcess(item.id_transaction, item.totalPrice, item.shippingCost)
          ) : (item.status === 'PENDING' ? (
            item.payment.paymentMethod !== null ? (
              handlePending(item.id_transaction, item.totalPrice, item.payment.paymentMethod, item.payment.paymentMethod.isVirtualAccount,
                item.payment.paymentMethod.bank, item.payment.virtualAccount)

            ) : (
              Alert.alert('Peringatan', 'Silahkan selesaikan pembayaran')
            )
          ) : (
            navigation.navigate('DetailHistory',{ historyID: item })
          )
          )
        }
      >
        <View>
          {item.payment && item.payment.paymentMethod !== null ? (
            <View>
              <Text style={styles.fontOthers}>{moment(item.payment.paymentMethod.createdAt).format('Do MMMM YY')}</Text>
            </View>
          ) : (
            <View>
              <Text style={styles.fontOthers}>Silahkan lakukan pembayaran!</Text>
            </View>
          )}
        </View>
        <View style={{ flexDirection: 'row',height: 0.1102 * height }}>
          <View style={{ alignSelf: 'center' }}>

            <Image
              style={styles.imgcard}
              source={{
                uri: `${item.products[0].poster}`
              }}
            />

          </View>

          <View style={styles.containerKanan}>
            <View>
              <Text style={styles.fontName}>{item.products[0].name}</Text>
              {productOthers > 1 ? (
                <View>
                  <Text style={styles.fontOthers}>+{productOthers} produk lain</Text>
                </View>
              ) : (
                <View>
                  <Text style={styles.fontOthers}> </Text>
                </View>)}
            </View>
            <View>
              <Text style={styles.fontTotal}>Total Belanja</Text>
              <NumberFormat value={item.totalPrice + item.shippingCost}
                displayType={'text'}
                prefix={'Rp. '}
                renderText={value => (
                  <Text style={styles.fontOthers}>{value} </Text>
                )}
                thousandSeparator={'.'} decimalSeparator={','}
              />
            </View>


            {item.status === 'DELIVERING' ? (
              <View style={styles.status}>
                <Text style={styles.fontStatus}>{item.status}</Text>
              </View>)
              : (<View style={styles.statusWaiting}>
                <Text style={styles.fontStatus}>{item.status}</Text>
              </View>)}
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  useEffect(() => {
    getToken();
    dispatch(getTransactionHistory());
    dispatch(getDataProfile());
  }, []);


  const getToken = async ()=>{
    const item = await AsyncStorage.getItem('token');
    setData({
      ...data,
      token: item });
  };

  const renderContent = () => (
    <View style={{ backgroundColor: '#fffaf4', height: height * 0.85, width: width }}>
      <View style={{ flex: 1 }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 0.2 }} />
          <View style={{ flex: 1 }}>
            <Text style={styles.title_input}>Halo, {profile.name} </Text>
          </View>
          <View style={{ flex: 0.2 }}>
            <TouchableOpacity hitSlop={styles.histSlop} style={styles.edit}
              onPress={()=>navigation.navigate('EditProfil',{ data:
              { username: profile.name,
                email: profile.email,
                password: profile.password,
                phonenumber: profile.phones,
                NIK: profile.ktp,
                KK: profile.kk } })}>
              <Feather name="edit" color="#DE3030" size={30} />
            </TouchableOpacity>
          </View>
        </View>

        {showProfile === true ?
          (
            <View style={styles.dataInput}>
              <View style={{ flexDirection: 'row' }}>
                <Feather name="mail" color="#DE3030" size={32} style={styles.icon}/>
                <View>
                  <Text style={styles.body_text}>Email</Text>
                  <Text style={styles.body_subtext}>{profile.email}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Feather name="sidebar" color="#DE3030" size={32} style={styles.icon}/>
                <View>
                  <Text style={styles.body_text}>KK</Text>
                  <Text style={styles.body_subtext}>{!profile.kk ? '-' : profile.kk}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Feather name="credit-card" color="#DE3030" size={32} style={styles.icon}/>
                <View>
                  <Text style={styles.body_text}>NIK</Text>
                  <Text style={styles.body_subtext}>{!profile.ktp ? '-' : profile.ktp}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Feather name="phone" color="#DE3030" size={32} style={styles.icon}/>
                <View style={{ flexDirection: 'column' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.body_text}>Nomor Handphone </Text>
                  </View>
                  <View>
                    {profile.phones?.length && profile.phones[0] !== null ? profile.phones.map((item,i)=>{
                      return (
                        <View key={i}>
                          <Text style={styles.body_subtext}>
                      Phone {i + 1} : {item.phone}</Text>
                        </View>
                      );
                    }) : <Text style={styles.body_subtext}>-</Text>
                    }
                  </View>
                </View>
              </View>
            </View>
          ) : (
            <View style={styles.dataTrans}>
              <Text style={styles.fontTitle}>Daftar Transaksi</Text>
              {dataHistory && Object.entries(dataHistory).length >= 1 ?
                (
                  <FlatList
                    data = {dataHistory.filter((value) => value.status === 'DELIVERING' || value.status === 'PROCESS' || value.status === 'PENDING')}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={item => item.id_transaction}
                  />
                )
                : (
                  <View
                    style={{
                      marginVertical: 30,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}>
                    <Image
                      style={{ width: 70, height: 70 }}
                      source={require('../../../assets/notFound.png')}
                    />
                    <Text
                      style={{
                        marginLeft: 10,
                        marginTop: 20,
                        color: 'grey',
                        fontSize: 16
                      }}>
                    belum ada transaksi :(
                    </Text>
                  </View>

                )
              }
            </View>
          ) }

      </View>
      <View style={{ flex: 1, marginTop: height * 0.5 }}>
        <View style={{ flexDirection: 'row', width: width }}>
          <View style={styles.button_home}>
            <TouchableOpacity onPress={()=>navigation.navigate('Home')}
              hitSlop={{ top: width * 0.05, bottom: width * 0.05, left: width * 0.08, right: width * 0.08 }}>
              <Feather name="home" color="#DE3030" size={30} />
            </TouchableOpacity>
          </View>
          <View style = {{ alignSelf: 'center' }}>
            <SwitchSelector
              options={[
                { label: 'Profil', value: true },
                { label: 'Transaksi', value: false }
              ]}
              initial={0}
              textColor={'#de3030'} //'#7a44cf'
              selectedColor={'white'}
              buttonColor={'#de3030'}
              height={55}
              style={styles.button}
              hasPadding
              onPress={value => setShowProfile(value)}/>
          </View>
          <View style={styles.button_logout}>
            <TouchableOpacity onPress={logOut}
              hitSlop={{ top: width * 0.05, bottom: width * 0.05, left: width * 0.08, right: width * 0.08 }}>
              <Feather name="log-out" color="#DE3030" size={30} />
            </TouchableOpacity>
          </View>
        </View>

      </View>
    </View>
  );
  const renderHeader = () => (
    <View style={{ flex: 1, height: height * 0.04 }}>
      <Image source={require('../../../assets/image12.png')} style={styles.logo} />
    </View>
  );

  const logOut = async () => {
    try {
      const item = await AsyncStorage.removeItem('token');
      if (item == null){
        navigation.reset({
          index: 0,
          routes: [{ name: 'WelcomeScreen' }]
        });
      }
    } catch (err) {
      throw err;
    }
  };

  const sheetRef = React.useRef(null);

  return (
    <>
      <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
        <View style={styles.container}>
          <ImageBackground source={require('../../../assets/profile.png')} style={styles.image} />
        </View>
      </LinearGradient>

      <BottomSheet
        ref={sheetRef}
        snapPoints={[height * 0.86, height * 0.5]}
        borderRadius={30}
        renderContent={renderContent}
        renderHeader={renderHeader}
        initialSnap={0}
        enabledBottomClamp={false}
        enabledContentGestureInteraction={false}
      />

    </>
  );
};
export default ProfileScreen;
