import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#De3030'
  },
  homeHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 0.0555 * width,
    marginRight: 0.0555 * width,
    marginTop: 0.0612 * height
  },
  fontHalo: {
    fontSize: 20,
    color: 'white',
    alignSelf: 'center',
    marginLeft: 10
  },
  panel: {
    height: height * 0.9,
    marginTop: -10,
    paddingVertical: 20,
    paddingHorizontal: 30,
    backgroundColor: '#FEFAF5'
  },
  header: {
    backgroundColor: '#FEFAF5',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 100,
    height: 5,
    borderRadius: 4,
    backgroundColor: '#F18F40',
    marginBottom: 10
  },
  fontSheet: {
    fontSize: 18,
    color: '#F18F40',
    fontWeight: 'bold'
  },
  fontNumber: {
    fontSize: 24,
    color: '#000000',
    fontWeight: 'bold'
  },
  topContainer: {
    height: 0.30580 * height,
    marginTop: 0.03254 * height
  },
  bottomContainer: {
    height: 0.2689 * height,
    marginTop: 0.0390 * height
  },
  menuContainer: {
    zIndex: 1,
    height: 0.2689 * height,
    width: 0.3888 * width,
    borderRadius: 20,
    alignContent: 'center',
    padding: 5,
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3
  },
  sheetContainer: {
    height: 0.57700 * height,
    justifyContent: 'space-around'
  },
  menuImage: {
    width: 0.3223 * width,
    height: 0.15741 * height,
    alignSelf: 'center',
    marginBottom: 10
  },
  homeImage: {
    width: 1 * width,
    height: 0.5 * height,
    alignSelf: 'center',
    marginBottom: 10
  },
  fontMenu: {
    fontSize: 16,
    color: '#000000',
    alignSelf: 'center',
    fontWeight: 'bold',
    marginBottom: 5
  },
  fontHarga: {
    fontSize: 14,
    color: '#DE3030',
    alignSelf: 'center',
    fontWeight: 'bold'
  },
  search: {
    borderRadius: 100,
    borderColor: '#dbdbdb',
    borderWidth: 1,
    padding: 10,
    margin: 10,
    width: width * 0.8,
    backgroundColor: 'white'
  }
});
export default styles;
