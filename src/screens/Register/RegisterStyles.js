import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    flex: 1
  },
  divider: {
    flex: 1,
    alignSelf: 'center',
    top: height / 10
  },
  header: {
    flex: 1,
    height: height * 0.3,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50
  },

  body: {
    flex: 1,
    height: height * 0.4,
    borderRadius: 30,
    paddingHorizontal: 20
  },
  footer: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    alignContent: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    fontSize: 30
  },
  forgot: {
    width: width * 0.9,
    height: height * 0.05,
    marginBottom: height * 0.03,
    textAlign: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 20
  },
  textInput: {
    width: width * 0.9,
    height: height * 0.05,
    textAlign: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 20
  },
  icon: {
    position: 'absolute',
    left: width * 0.06,
    top: height * 0.015
  },
  modalicon: {
    position: 'absolute',
    alignItems: 'flex-end',
    alignSelf: 'flex-end'
  },
  check: {
    position: 'absolute',
    left: width - 80,
    bottom: height * 0.015
  },
  icons: {
    position: 'absolute',
    alignSelf: 'flex-end'
  },
  action: {
    marginTop: height * 0.04
  },
  actions: {
    marginTop: height * 0.2
  },
  textPrivate: {
    justifyContent: 'flex-end',
    fontSize: 30,
    fontWeight: 'bold',
    color: 'white',
    alignContent: 'flex-end',
    alignSelf: 'flex-end',
    marginRight: width * 0.1
  },
  text: {
    justifyContent: 'flex-end',
    fontSize: 12,
    fontWeight: 'bold',
    color: 'white',
    alignContent: 'flex-end',
    alignSelf: 'flex-end',
    marginTop: height * 0.02
  },
  text_footer: {
    flexDirection: 'row',
    alignContent: 'flex-end',
    alignSelf: 'flex-end'
  },
  pictureView: {
    width: width - 40,
    height: width - 40,
    marginBottom: height * 0.02
  },
  picturesView: {
    marginTop: height * 0.05,
    width: width,
    height: width * 0.5
  },
  eye: {
    position: 'absolute',
    left: width * 0.8,
    top: height * 0.015
  },
  Title: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 24,
    fontFamily: 'arial',
    marginLeft: 10
  },
  iconAwesome: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: height * 0.03
  },
  iconicon: {
    paddingLeft: width * 0.08,
    paddingTop: height * 0.02,
    flexDirection: 'row'
  },
  Subtitle: {
    color: 'black',
    fontSize: 16,
    fontFamily: 'arial',
    marginLeft: 10,
    marginTop: 5,
    marginBottom: 10
  },
  textPrivates: {
    justifyContent: 'center',
    fontSize: 20,
    paddingTop: height * 0.01,
    fontWeight: 'bold',
    color: '#DE3030',
    alignContent: 'center',
    alignSelf: 'center'
  },
  title_input: {
    color: '#F2F2F2',
    fontWeight: 'bold',
    fontSize: 18,
    fontFamily: 'arial',
    marginLeft: 10
  },
  textError: {
    color: 'white',
    fontSize: 11,
    alignSelf: 'center',
    fontFamily: 'arial',
    paddingTop: height * 0.015,
    marginLeft: 10
  },
  button: {
    alignItems: 'center',
    padding: height * 0.01
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  color_textPrivate: {
    color: 'grey'
  },
  panelText: {
    padding: 5,
    fontSize: 10,
    color: 'white',
    borderRadius: 10,
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    marginVertical: 5
  },
  modal: {
    fontSize: 22,
    fontWeight: 'normal',
    color: 'grey',
    alignContent: 'center',
    textAlign: 'center',
    marginTop: height * 0.02
  },
  modalbutton: {
    width: width * 0.3,
    height: height * 0.04,
    backgroundColor: '#FCE5B9',
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: height * 0.02
  }

});

export default styles;
