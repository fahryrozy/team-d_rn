import React, { } from 'react';
import { View, Text, Image, ScrollView, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, TextInput, TouchableOpacity, Alert } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import * as Animatable from 'react-native-animatable';
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector,useDispatch } from 'react-redux';

import { goRegister } from '../../store/actions/profileAction';
import styles from '../Register/RegisterStyles';

const RegistersScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.profileStore.isLoading);
  const [data, setData] = React.useState({
    username: null,
    password: null,
    email: null,
    confirm_password: '',
    phonenumber: null,
    check_phonenumberChange: false,
    hidePsw: null,
    check_textInputChange: false,
    secureTextEntry: true,
    onError: false,
    onMessage: null,
    errorMsg: null,
    check_UsernameChange: false,
    check_emailInputChange: false,
    check_passwordChange: Boolean,
    name: ''
  });

  const loginClick = () => {
    navigation.navigate('Login');
  };

  const usernameChange = (val) => {
    let rjx = /^(?:[A-Za-z ]+|\d+)$/;
    let isValid = rjx.test(val);
    if (val.length !== 0 && isValid === true) {
      setData({
        ...data,
        username: val,
        check_UsernameChange: true
      });
    } else {
      setData({
        ...data,
        username: val,
        check_UsernameChange: false
      });
    }
  };
  const emailInputChange = (val) => {
    let reg = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/;
    let isValid = reg.test(val);
    if (val.trim().length !== '' && isValid === true) {
      setData({
        ...data,
        email: val,
        check_emailInputChange: true
      });
    } else {
      setData({
        ...data,
        email: val,
        check_emailInputChange: false
      });
    }
  };
  const phonenumberChange = (val) => {
    const awal = val.substring(0, 4);
    switch (true) {
      case (val.length > 9 && val.length <= 12 && (awal === '0811' ||
      awal === '0812' ||
      awal === '0813' ||
      awal === '0821' ||
      awal === '0822' ||
      awal === '0852' ||
      awal === '0853' ||
      awal === '0823' ||
      awal === '0851')):
      case (val === ''):
        setData({
          ...data,
          phonenumber: val,
          check_phonenumberChange: true
        });
        break;

      default:
        setData({
          ...data,
          phonenumber: val,
          check_phonenumberChange: false
        });
        break;
    }
  };

  const passwordChange = (val) => {
    if (val.length > 4) {
      setData({
        ...data,
        password: val,
        check_passwordChange: true
      });
    } else {
      setData({
        ...data,
        check_passwordChange: false
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      hidePsw: !data.hidePsw
    });
  };

  const validate_field = () => {
    if (data.check_UsernameChange === false) {
      Alert.alert('Nama harus menggunakan alphabet');
      return false;
    }
    else if (data.check_emailInputChange === false) {
      Alert.alert('Format email salah');
      return false;
    }
    else if (data.check_passwordChange === false) {
      Alert.alert('Format password salah');
      return false;
    }
    else if (data.check_phonenumberChange === false && data.phonenumber !== null ) {
      Alert.alert('Masukkan No Telkomsel');
      return false;
    }
    else {
      return true;
    }
  };

  const register_success = () => {
    if (validate_field()) {
      SignUp();
    }
  };

  const SignUp = () => {
    let input = {
      name: data.username,
      email: data.email,
      password: data.password
    };
    if (data.phonenumber !== '' || data.phonenumber) {
      input.phone = data.phonenumber;
    }
    dispatch(goRegister(input));
  };

  return (
    <KeyboardAvoidingView
      behavior="padding"
      style={{ flex: 1 }}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
          <ScrollView >
            <View style={styles.header}>
              <View style={styles.divider}>
                <Text style={styles.text_header}>Register Now!</Text>
                <Image
                  style={styles.pictureView}
                  source={require('../../../assets/home.png')} />
              </View>
            </View>

            <View style={styles.body}>
              <Animatable.View animation="fadeInUpBig">
                <View style={styles.action}>
                  <TextInput
                    placeholder="Masukkan Nama"
                    placeholderTextColor="#DE3030"
                    style={styles.textInput}
                    onChangeText={(val) => usernameChange(val)}
                  />
                  <Feather name="user" color="#DE3030" size={20} style={styles.icon} />
                  {data.check_UsernameChange ?
                    <Animatable.View
                      animation="bounceIn"
                    >
                      <Feather
                        name="check-circle"
                        color="green"
                        size={20}
                        style={styles.check}
                      />
                    </Animatable.View>
                    : null}
                </View>

                <View style={styles.action}>
                  <TextInput
                    placeholder="Masukkan Email"
                    placeholderTextColor="#DE3030"
                    autoCapitalize="none"
                    keyboardType={'email-address'}
                    textContentType={'emailAddress'}
                    style={styles.textInput}
                    onChangeText={(val) => emailInputChange(val)}
                  />
                  <Feather name="mail" color="#DE3030" size={20} style={styles.icon} />
                  {data.check_emailInputChange ?
                    <Animatable.View
                      animation="bounceIn"
                    >
                      <Feather
                        name="check-circle"
                        color="green"
                        size={20}
                        style={styles.check}
                      />
                    </Animatable.View>
                    : null}
                </View>
                <View style={styles.action}>
                  <TextInput
                    placeholder="Masukkan Password"
                    placeholderTextColor="#DE3030"
                    secureTextEntry={!data.hidePsw}
                    autoCapitalize="none"
                    style={styles.textInput}
                    onChangeText={(val) => passwordChange(val)}
                  />
                  <Feather name="key" color="#DE3030" size={20} style={styles.icon} />
                  <TouchableOpacity disabled={data.isLoading} onPress={updateSecureTextEntry} style={styles.eye}>
                    {data.hidePsw ?
                      <Feather name="eye" color="black" size={20} />
                      : <Feather name="eye-off" color="black" size={20} />
                    }
                  </TouchableOpacity>
                  {data.check_passwordChange ?
                    null
                    : <Text style={styles.textError}>Password minimum contain 5 characters!</Text>}

                </View>
                <View style={styles.action}>
                  <TextInput
                    placeholder="Nomor Telepon"
                    placeholderTextColor="#DE3030"
                    style={styles.textInput}
                    keyboardType={'phone-pad'}
                    onChangeText={(val) => phonenumberChange(val)}
                  />
                  <Feather name="phone" color="#DE3030" size={20} style={styles.icon} />
                  {data.check_phonenumberChange && data.phonenumber !== '' ?
                    <Animatable.View
                      animation="bounceIn"
                    >
                      <Feather
                        name="check-circle"
                        color="green"
                        size={20}
                        style={styles.check}
                      />
                    </Animatable.View>
                    : null}
                  <Text style={styles.panelText}>Opsional - jika memiliki nomor Telkomsel</Text>
                </View>
              </Animatable.View>
            </View>
            <View style={styles.footer}>
              <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>

                <TouchableOpacity onPress={register_success}>
                  <View>

                    <Text style={styles.textPrivate}>Daftar </Text>
                    <Feather name="arrow-right-circle" color="white" size={40} style={styles.icons} />
                  </View>

                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
                <TouchableOpacity onPress={() => loginClick()}>
                  <Text style={styles.text}>Sudah punya akun? Masuk </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
          <Spinner
            visible={isLoading}
            textContent={'Loading...'}
            textStyle={styles.spinnerTextStyle}
            animation="fade"
          />
        </LinearGradient>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

export default RegistersScreen;
