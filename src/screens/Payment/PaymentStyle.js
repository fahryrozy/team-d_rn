import { Dimensions, StyleSheet } from 'react-native';

import { theme } from '../../theme';

const { height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
    alignItems: 'center'
  },
  panel: {
    height: height * 0.925,
    paddingVertical: 20,
    paddingBottom: height * 0.1,
    paddingHorizontal: 20,
    backgroundColor: theme.colors.bottomSheet
  },
  header: {
    backgroundColor: theme.colors.bottomSheet,
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 100,
    height: 8,
    borderRadius: 4,
    backgroundColor: theme.colors.secondary,
    marginBottom: 5
  },
  panelTitle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    fontSize: theme.sizes.title,
    fontWeight: 'bold',
    color: theme.colors.primary,
    width: '75%',
    textAlign: 'center'
  },
  panelPrice: {
    flex: 1.5,
    paddingVertical: 10
  },
  label: {
    fontSize: theme.sizes.header,
    fontWeight: 'bold',
    color: theme.colors.secondary,
    marginVertical: 5
  },
  price: {
    fontWeight: 'bold',
    fontSize: 30,
    color: theme.colors.tertiary
  },
  panelPayment: {
    flex: 9,
    paddingVertical: 10
  }
});
export default styles;
