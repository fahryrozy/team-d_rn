import {
  Dimensions,
  StyleSheet
} from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#De3030'
  },
  fontHalo: {
    fontSize: 20,
    color: 'white',
    alignSelf: 'center',
    textAlign: 'center',
    marginLeft: 20
  },
  homeHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 0.0555 * width,
    marginRight: 0.0555 * width,
    marginTop: 0.0612 * height
  },
  search: {
    borderRadius: 100,
    borderColor: '#dbdbdb',
    borderWidth: 1,
    margin: 5,
    width: width * 0.9,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    paddingHorizontal: 10
  },
  item: {
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 100
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    margin: 5
  },
  GantiHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 0.0555 * width,
    marginRight: 0.0555 * width,
    marginTop: 0.0612 * height,
    marginBottom: 0.0212 * height
  },
  fontGanti: {
    fontSize: 20,
    color: 'white',
    alignSelf: 'center',
    marginLeft: 0.1942 * width,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  MainContainer: {
    flex: 1,
    padding: 12
  },
  selectedTextContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  selectedTextStyle: {
    textAlign: 'center',
    fontSize: 18
  },
  AutocompleteStyle: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },
  InputStyle: {
    borderColor: '#dbdbdb',
    zIndex: 1
  },
  ListStyle: {
    marginTop: 20,
    borderColor: 'white'
  },
  SearchBoxTextItem: {
    marginLeft: 5,
    marginBottom: 5,
    fontSize: 16,
    paddingTop: 4,
    fontWeight: 'bold'
  },
  SearchBoxPriceItem: {
    margin: 5,
    fontSize: 14,
    paddingTop: 4
  },
  header: {
    backgroundColor: '#FEFAF5',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 100,
    height: 5,
    borderRadius: 4,
    backgroundColor: '#F18F40',
    marginBottom: 10
  },
  panel: {
    height: height * 0.85,
    paddingVertical: 20,
    paddingHorizontal: 10,
    backgroundColor: '#FEFAF5'
  },
  homeImage: {
    width: 1 * width,
    height: 0.5 * height,
    alignSelf: 'center',
    marginBottom: 10
  }
});
export default styles;
