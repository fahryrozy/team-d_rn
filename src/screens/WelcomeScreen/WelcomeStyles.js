import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    flex: 1
  },
  ticker: {
    flexDirection: 'row',
    width: width - 20,
    left: width * 0.03,
    marginBottom: height * 0.01,
    backgroundColor: 'white'
  },
  icon: {
    position: 'absolute',
    right: width * 0.06,
    marginRight: width * 0.5
  },
  footer: {
    flex: 0.8
  },
  logo: {
    borderRadius: 10,
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginTop: height * 0.005,
    marginBottom: height * 0.03,
    flexDirection: 'row'
  },
  text: {
    marginLeft: width * 0.03,
    marginTop: height * 0.005,
    marginBottom: height * 0.03,
    fontSize: 22,
    fontWeight: 'bold',
    fontFamily: 'arial',
    color: '#FFFFFF',
    alignSelf: 'center',
    flexDirection: 'row'
  },
  text_logo: {
    alignSelf: 'center',
    flexDirection: 'row'
  },
  text_footer: {
    borderRadius: 10,
    fontSize: 15,
    alignSelf: 'center'
  },
  buttonSignup: {
    borderRadius: 20,
    alignSelf: 'center'
  },
  buttonSignIn: {
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: height * 0.02
  }
});

export default styles;
