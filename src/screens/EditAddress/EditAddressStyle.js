import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DE3030',
    alignItems: 'center'
  },
  panel: {
    flex: 1,
    paddingVertical: 20,
    paddingHorizontal: 20
  },
  header: {
    backgroundColor: '#FEFAF5',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 100,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#F18F40',
    marginBottom: 5
  },
  panelTitle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#FEFAF5',
    width: '75%',
    textAlign: 'center'
  },
  panelContent: { flex: 9, paddingVertical: 10 },

  addressLabel: { flex: 0.125 },
  addressArea: { flex: 1.2, justifyContent: 'flex-start', alignItems: 'center', zIndex: 1 },
  addressDetail: { flex: 0.4 },

  inputContainer: {
    backgroundColor: '#FFF',
    borderRadius: 50,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    height: height * 0.06,
    width: width * 0.9
  },

  provinceContainer: { top: height * 0.015, position: 'absolute', zIndex: 99 },
  cityContainer: { top: height * 0.085, position: 'absolute', zIndex: 98 },
  cityContainerDisabled: {
    top: height * 0.085,
    position: 'absolute',
    backgroundColor: '#FFF',
    borderRadius: 50,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    height: height * 0.06,
    width: width * 0.9
  },
  regencyContainer: { top: height * 0.155, position: 'absolute', zIndex: 97 },
  regencyContainerDisabled: {
    top: height * 0.155,
    position: 'absolute',
    backgroundColor: '#FFF',
    borderRadius: 50,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    height: height * 0.06,
    width: width * 0.9
  },
  districtContainer: {
    top: height * 0.225,
    position: 'absolute',
    backgroundColor: '#FFF',
    borderRadius: 50,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    height: height * 0.06,
    width: width * 0.9
  },
  postalCodeContainer: {
    top: height * 0.295,
    position: 'absolute',
    backgroundColor: '#FFF',
    borderRadius: 50,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    height: height * 0.06,
    width: width * 0.9
  },
  detailAddressContainer: {
    top: height * 0.365,
    backgroundColor: '#FFF',
    borderRadius: 50,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    height: height * 0.06,
    width: width * 0.89
  },
  sharelocContainer: {
    top: height * 0.45,
    position: 'absolute',
    backgroundColor: '#FFF',
    borderRadius: 10,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    paddingHorizontal: 20,
    height: height * 0.04
  },
  autoInputContainerStyle: { borderWidth: 0, padding: 0 },
  autoListContainerStyle: { borderWidth: 0, backgroundColor: '#FFF', borderRadius: 5, marginTop: -5 },
  listItemAutoInput: { borderBottomColor: '#B4ACAC', borderBottomWidth: 1, marginVertical: 10, zIndex: 100 },

  iconInput: { alignSelf: 'center',marginLeft: width * 0.001, paddingLeft: 10 },
  iconLocation: { alignSelf: 'center', paddingHorizontal: 5 },
  iconVerified: { width: 35, alignItems: 'center', justifyContent: 'center', paddingHorizontal: 2 },

  textInput: {
    marginLeft: width * 0.04,
    width: width * 0.65,
    backgroundColor: 'white',
    paddingVertical: 5,
    borderRadius: 5,
    paddingHorizontal: 5 ,
    height: height * 0.06 - 12
  },

  textArea: {
    marginLeft: width * 0.04,
    width: width * 0.65,
    backgroundColor: 'white',
    paddingVertical: 5,
    borderRadius: 5,
    height: height * 0.05,
    justifyContent: 'flex-start',
    textAlignVertical: 'top'
  },

  icon: {
    position: 'absolute',
    left: width * 0.03,
    top: height * 0.015,
    backgroundColor: 'green'
  },

  pictureViewIOS: {
    marginTop: 0,
    marginBottom: -40,
    alignSelf: 'center',
    width: 200,
    height: 200
  },

  pictureViewAndroid: {
    marginTop: -40,
    marginBottom: -40,
    alignSelf: 'center',
    width: 200,
    height: 200
  },

  buttonSave: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  buttonTitle: {
    paddingHorizontal: 10,
    color: '#FFF',
    fontSize: 20
  }
});
export default styles;
