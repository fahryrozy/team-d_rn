import React, { useState, useEffect } from 'react';
import {
  Text,
  TextInput,
  Alert,
  Image,
  View,
  Platform,
  TouchableOpacity
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useSelector, useDispatch } from 'react-redux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import Autocomplete from 'react-native-autocomplete-input';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Geolocation from 'react-native-geolocation-service';

import * as RootNavigation from '../../navigation/RootNavigation';
import { getCity, setCity } from '../../store/actions/cityAction';
import { getProvince, setProvince } from '../../store/actions/provinceAction';
import { getRegency, setRegency } from '../../store/actions/regencyAction';
import { setDistrict } from '../../store/actions/districtAction';
import { getDistrict } from '../../store/actions/districtAction';
import { hasLocationPermission, requestPermissions } from '../../utils/location';
import { myLocation } from '../../store/actions/longlatAction';
import { editAddressAction } from '../../store/actions/addressAction';

import styles from './EditAddressStyle';

const EditAddressScreen = () => {
  const editAddress = useSelector((state) => state.addressStore.editAddress);
  const [addressName, setAddressName] = useState(editAddress.addressName);
  const [addressDetail, setAddressDetail] = useState(editAddress.street);
  const [inputProvince, setInputProvince] = useState(editAddress.province);
  const [inputCity, setInputCity] = useState(editAddress.state);
  const [inputRegency, setInputRegency] = useState(editAddress.regency);
  const [inputDistrict, setInputDistrict] = useState(editAddress.district);
  const [postalCode, setPostalCode] = useState(editAddress.postalCode);
  const [isDistrictEditable, setIsDistrictEditable] = useState(false);

  const dispatch = useDispatch();
  const listProvince = useSelector((state) => state.addressStore.province);
  const listCity = useSelector((state) => state.addressStore.city);
  const listRegency = useSelector((state) => state.addressStore.regency);
  const long = useSelector((state) => state.addressStore.long);
  const lat = useSelector((state) => state.addressStore.lat);

  const selectedProvince = useSelector((state) => state.addressStore.selectedProvince);
  const selectedCity = useSelector((state) => state.addressStore.selectedCity);
  const selectedRegency = useSelector((state) => state.addressStore.selectedRegency);

  const [hideProvinceOption, setHideProvinceOption] = useState(false);
  const [hideCityOption, setHideCityOption] = useState(false);
  const [hideRegencyOption, setHideRegencyOption] = useState(false);

  const [isProvinceError, setIsProvinceError] = useState(false);
  const [isCityError, setIsCityError] = useState(false);
  const [isRegencyError, setIsRegencyError] = useState(false);

  const setLocationHandler = () => {
    hasLocationPermission();
    requestPermissions();
    Geolocation.getCurrentPosition(position => dispatch(myLocation(position.coords.longitude, position.coords.latitude)));
  };

  useEffect(() => {
    dispatch(getProvince());
  }, []);

  const inputAddressHandler = () => {
    if (isValidData(addressName) && isValidData(addressDetail) &&
      Object.entries(selectedProvince).length >= 1 &&
      Object.entries(selectedCity).length >= 1 &&
      Object.entries(selectedRegency).length >= 1 &&
      isValidData(inputDistrict) &&
      isValidData(postalCode)) {
      const addressInfo = {
        id: editAddress.id,
        addressName: addressName,
        street: addressDetail,
        province: selectedProvince.name,
        district: inputDistrict,
        regency: selectedRegency.name,
        state: selectedCity.name,
        postalCode: postalCode,
        long: long ? long.toString() : null,
        lat: lat ? lat.toString() : null
      };
      dispatch(editAddressAction(addressInfo));
    }
    else {Alert.alert('Error', 'Data Belum Lengkap');}
  };

  const findData = (query, data) => {
    if (query === '') {return [];}
    const regex = new RegExp(`${query}`, 'i');
    const dataBE = data.filter(item => item.name.search(regex) >= 0);
    return dataBE;
  };

  const isValidData = (query, data) => {
    if (data) {return data.find(item => item.name === query);}
    else {
      if (query.length >= 5) {return true;}
      else {return false;}
    }
  };

  const onProvinceChange = (val) => {
    setInputProvince(val);
    setHideProvinceOption(false);
    dispatch(setProvince({}));
    dispatch(setCity({}));
    dispatch(setRegency({}));
    dispatch(setDistrict({}));
  };

  const onProvinceChangeComplete = () => {
    if (isValidData(inputProvince, listProvince)) {
      setIsProvinceError(false);
      setHideProvinceOption(true);
      dispatch(getCity(isValidData(inputProvince, listProvince).id));
      dispatch(setProvince(isValidData(inputProvince, listProvince)));
    }
    else {
      setIsProvinceError(true);
      setHideProvinceOption(true);
    }
  };

  const onCityChange = (val) => {
    setInputCity(val);
    setHideCityOption(false);
    dispatch(setCity({}));
    dispatch(setRegency({}));
    dispatch(setDistrict({}));
  };

  const onCityChangeComplete = () => {
    if (isValidData(inputCity, listCity)) {
      setIsCityError(false);
      setHideCityOption(true);
      dispatch(getRegency(isValidData(inputCity, listCity).id));
      dispatch(setCity(isValidData(inputCity, listCity)));
    }
    else {
      setIsCityError(true);
      setHideCityOption(true);
    }
  };

  const onRegencyChange = (val) => {
    setInputRegency(val);
    dispatch(setRegency({}));
    dispatch(setDistrict({}));
    setHideRegencyOption(false);
    setIsDistrictEditable(false);
  };

  const onRegencyChangeComplete = () => {
    if (isValidData(inputRegency, listRegency)) {
      setIsRegencyError(false);
      setHideRegencyOption(true);
      dispatch(getDistrict(isValidData(inputRegency, listRegency).id));
      dispatch(setRegency(isValidData(inputRegency, listRegency)));
      setIsDistrictEditable(true);
    }
    else {
      setIsRegencyError(true);
      setHideRegencyOption(true);
    }
  };

  const selectProvince = ({ item }) => (
    <TouchableOpacity style={styles.listItemAutoInput} onPress={()=> {
      setInputProvince(item.name);
      setHideProvinceOption(true);
      dispatch(getCity(item.id));
      dispatch(setProvince(item));
    }
    }>
      <Text> {item.name} </Text>
    </TouchableOpacity>
  );

  const selectCity = ({ item }) => (
    <TouchableOpacity style={styles.listItemAutoInput} onPress={()=>{
      setInputCity(item.name);
      setHideCityOption(true);
      dispatch(getRegency(item.id));
      dispatch(setCity(item));
    }
    }>
      <Text> {item.name} </Text>
    </TouchableOpacity>
  );

  const selectRegency = ({ item }) => (
    <TouchableOpacity style={styles.listItemAutoInput} onPress={() => {
      setInputRegency(item.name);
      setHideRegencyOption(true);
      dispatch(getDistrict(item.id));
      dispatch(setRegency(item));
      setIsDistrictEditable(true);
    }
    }>
      <Text> {item.name} </Text>
    </TouchableOpacity>
  );

  return (
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
      <View style={styles.panel}>
        <Image style={Platform.OS === 'ios' ? styles.pictureViewIOS : styles.pictureViewAndroid} source={require('../../../assets/editprofile.png')} />
        <View style={styles.panelTitle}>
          <TouchableOpacity onPress={()=>RootNavigation.pop()}>
            <Ionicons name="chevron-back-circle" size={30} color={'#FFF'} style={{ paddingRight: 10, opacity: 0.5 }} />
          </TouchableOpacity>
          <Text style={styles.title}>Tambah Alamat</Text>
        </View>

        <View style={styles.panelContent}>
          <View style={styles.addressLabel}>
            <View style={styles.inputContainer}>
              <FontAwesome name="address-book" size={24} color={'#DE3030'} style={styles.iconInput} />
              <TextInput placeholder="Nama Alamat" placeholderTextColor = "#B4ACAC"
                style={styles.textInput}
                value={addressName}
                onChangeText={(val) => setAddressName(val)}
              />
              <View style={styles.iconVerified}>
                {isValidData(addressName) && <MaterialCommunityIcons name="check-all" size={20} color={'#DE3030'}/>}
              </View>
            </View>
          </View>

          <View style={styles.addressArea}>
            <View style={styles.provinceContainer}>
              <Autocomplete keyExtractor={(item, index) => index.toString()}
                inputContainerStyle={styles.autoInputContainerStyle}
                listContainerStyle={styles.autoListContainerStyle}
                listStyle={{ borderWidth: 0 }}
                hideResults={hideProvinceOption}
                data={findData(inputProvince, listProvince)}
                renderTextInput={()=>(
                  <View style={styles.inputContainer}>
                    <MaterialCommunityIcons name="city" size={24} color={'#DE3030'} style={styles.iconInput} />
                    <TextInput placeholder="Provinsi"
                      placeholderTextColor = "#B4ACAC"
                      value={inputProvince}
                      style={styles.textInput}
                      onChangeText={onProvinceChange}
                      onEndEditing={onProvinceChangeComplete}
                    />
                    <View style={styles.iconVerified}>
                      {isValidData(inputProvince, listProvince) && <MaterialCommunityIcons name="check-all" size={20} color={'#DE3030'}/>}
                      {isProvinceError && !isValidData(inputProvince, listProvince) && <MaterialIcons name="error" size={20} color={'#DE3030'}/>}
                    </View>
                  </View>
                )}
                renderItem={selectProvince}
              />
            </View>

            {Object.entries(selectedProvince).length >= 1 ?
              <View style={styles.cityContainer}>
                <Autocomplete keyExtractor={(item, index) => index.toString()}
                  inputContainerStyle={styles.autoInputContainerStyle}
                  listContainerStyle={styles.autoListContainerStyle}
                  listStyle={{ borderWidth: 0 }}
                  renderSeparator={()=>(<Text>Separator</Text>)}
                  hideResults={hideCityOption}
                  data={findData(inputCity, listCity)}
                  renderTextInput={()=>(
                    <View style={styles.inputContainer}>
                      <MaterialCommunityIcons name="city-variant" size={24} color={'#DE3030'} style={styles.iconInput} />
                      <TextInput placeholder="Kabupaten / Kota"
                        placeholderTextColor = "#B4ACAC"
                        value={inputCity} style={styles.textInput}
                        onChangeText={onCityChange}
                        onEndEditing={onCityChangeComplete}
                      />
                      <View style={styles.iconVerified}>
                        {isValidData(inputCity, listCity) && <MaterialCommunityIcons name="check-all" size={20} color={'#DE3030'}/>}
                        {isCityError && !isValidData(inputCity, listCity) && <MaterialIcons name="error" size={20} color={'#DE3030'}/>}
                      </View>
                    </View>
                  )}
                  renderItem={selectCity}
                />
              </View>
              : <View style={styles.cityContainerDisabled}>
                <MaterialCommunityIcons name="city-variant" size={24} color={'#DE3030'} style={styles.iconInput} />
                <TextInput placeholder="Kabupaten / Kota"
                  placeholderTextColor = "#B4ACAC"
                  value={inputCity} style={styles.textInput}
                  editable={false}
                />
                <View style={styles.iconVerified} />
              </View>
            }

            {Object.entries(selectedCity).length >= 1 ?
              <View style={styles.regencyContainer}>
                <Autocomplete keyExtractor={(item, index) => index.toString()}
                  inputContainerStyle={styles.autoInputContainerStyle}
                  listContainerStyle={styles.autoListContainerStyle}
                  listStyle={{ borderWidth: 0 }}
                  hideResults={hideRegencyOption}
                  data={findData(inputRegency, listRegency)}
                  renderTextInput={()=>(
                    <View style={styles.inputContainer}>
                      <MaterialCommunityIcons name="home-city" size={24} color={'#DE3030'} style={styles.iconInput} />
                      <TextInput placeholder="Kecamatan"
                        placeholderTextColor = "#B4ACAC"
                        value={inputRegency} style={styles.textInput}
                        onChangeText={onRegencyChange}
                        onEndEditing={onRegencyChangeComplete}
                      />

                      <View style={styles.iconVerified}>
                        {isValidData(inputRegency, listRegency) && <MaterialCommunityIcons name="check-all" size={20} color={'#DE3030'} />}
                        {isRegencyError && !isValidData(inputRegency, listRegency) && <MaterialIcons name="error" size={20} color={'#DE3030'}/>}
                      </View>
                    </View>
                  )}
                  renderItem={selectRegency}
                />
              </View>
              : <View style={styles.regencyContainerDisabled}>
                <MaterialCommunityIcons name="home-variant" size={24} color={'#DE3030'} style={styles.iconInput} />
                <TextInput placeholder="Kecamatan"
                  placeholderTextColor = "#B4ACAC"
                  value={inputRegency} style={styles.textInput}
                  editable={false}
                />
                <View style={styles.iconVerified} />
              </View>
            }
            <View style={styles.districtContainer}>
              <MaterialCommunityIcons name="office-building" size={24} color={'#DE3030'} style={styles.iconInput} />
              <TextInput placeholder="Kelurahan"
                placeholderTextColor = "#B4ACAC"
                value={inputDistrict} style={styles.textInput}
                onChangeText={(val)=>setInputDistrict(val)}
                editable= {isDistrictEditable}
              />
              <View style={styles.iconVerified}>
                {isValidData(inputDistrict) && <MaterialCommunityIcons name="check-all" size={20} color={'#DE3030'}/>}
              </View>
            </View>
            <View style={styles.postalCodeContainer}>
              <FontAwesome name="address-book" size={24} color={'#DE3030'} style={styles.iconInput} />
              <TextInput placeholder="Kode Pos" placeholderTextColor = "#B4ACAC"
                style={styles.textInput}
                value={postalCode}
                keyboardType="number-pad" onChangeText={(val) => setPostalCode(val)}/>
              <View style={styles.iconVerified}>
                {isValidData(postalCode) && <MaterialCommunityIcons name="check-all" size={20} color={'#DE3030'}/>}
              </View>
            </View>

            <View style={styles.detailAddressContainer}>
              <FontAwesome name="home" size={24} color={'#DE3030'} style={styles.iconInput} />
              <TextInput placeholder="Nama jalan / gedung / nomor "
                autoCapitalize="none" placeholderTextColor = "#B4ACAC"
                value={addressDetail}
                style={styles.textInput} onChangeText={(val) => setAddressDetail(val)}
              />
              <View style={styles.iconVerified}>
                {isValidData(addressDetail) && <MaterialCommunityIcons name="check-all" size={20} color={'#DE3030'}/>}
              </View>
            </View>

            <TouchableOpacity style={styles.sharelocContainer} onPress={setLocationHandler}>
              <MaterialIcons name="add-location" size={24}color={long && lat ? '#DE3030' : '#B4ACAC'} style={styles.iconLocation} />
              <Text style={long && lat ? { color: '#000' } : { color: '#B4ACAC' }}>Location</Text>
            </TouchableOpacity>

          </View>

          <TouchableOpacity style={styles.buttonSave} onPress={()=>inputAddressHandler()}>
            <MaterialCommunityIcons name="content-save-move" size={54} color={'#FFF'} style={styles.iconInput} />
            <Text style={styles.buttonTitle}>Simpan</Text>
          </TouchableOpacity>
        </View>
      </View>
    </LinearGradient>
  );
};

export default EditAddressScreen;
