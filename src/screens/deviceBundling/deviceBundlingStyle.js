import { Dimensions, StatusBar, StyleSheet } from 'react-native';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0
  },
  item: {
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 100
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    margin: 5
  },
  GantiHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 0.0555 * width,
    marginRight: 0.0555 * width,
    marginTop: 0.0612 * height,
    marginBottom: 0.0212 * height
  },
  fontGanti: {
    fontSize: 20,
    color: 'white',
    alignSelf: 'center',
    marginLeft: 0.15 * width,
    fontWeight: 'bold',
    textAlign: 'center'
  }
});
export default styles;

