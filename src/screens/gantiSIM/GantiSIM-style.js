import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#de3030'
  },
  GantiHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 0.0555 * width,
    marginRight: 0.0555 * width,
    marginTop: 0.0612 * height
  },
  fontGanti: {
    fontSize: 20,
    color: 'white',
    alignSelf: 'center',
    marginLeft: 0.09420 * width,
    fontWeight: 'bold'

  },
  gantiSimImage: {
    width: 1 * width,
    height: 0.5 * height,
    alignSelf: 'center',
    marginBottom: 10
  },
  panel: {
    height: height * 0.85,
    paddingVertical: 20,
    paddingHorizontal: 30,
    backgroundColor: '#FFFAF4'
  },
  header: {
    backgroundColor: '#FEFAF5',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10
  },
  fontSheet: {
    fontSize: 20,
    color: '#F18F40',
    fontWeight: 'bold'
  },
  inputContainer: {
    height: height * 0.25,
    marginTop: height * 0.03794,
    justifyContent: 'space-around'
  },
  bottomContainer: {
    height: height * 0.29,
    marginTop: height * 0.14732,
    alignSelf: 'center'
  },
  inputView: {
    width: width * 0.8864,
    height: height * 0.064732,
    backgroundColor: '#FFFFFF',
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#F18F40',
    flexDirection: 'row',
    alignContent: 'center'
  },
  buttonSubmit: {
    width: width * 0.5217,
    height: height * 0.064732,
    backgroundColor: '#DE3030',
    borderRadius: 100,
    justifyContent: 'center',
    alignContent: 'center'
  },
  fontSubmit: {
    fontSize: 20,
    color: '#FFFFFF',
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  fontInput: {
    fontSize: 20,
    color: '#B4ACAC',
    alignSelf: 'center',
    marginLeft: width * 0.050724
  },
  iconInput: {
    alignSelf: 'center',
    marginLeft: width * 0.050724
  },
  textInput: {
    marginLeft: width * 0.049724,
    width: width * 0.70,
    height: height * 0.061732,
    fontSize: 20 ,
    backgroundColor: 'white',
    borderRadius: 20
  }

});
export default styles;
