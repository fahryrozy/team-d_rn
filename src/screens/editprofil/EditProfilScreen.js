import React, { useState, useEffect } from 'react';
import { View, Text, Image, Dimensions, Modal, TextInput, TouchableOpacity, Alert } from 'react-native';
import { HelperText } from 'react-native-paper';
import Feather from 'react-native-vector-icons/Feather';
const { width, height } = Dimensions.get('window');
import * as Animatable from 'react-native-animatable';
import { useSelector, useDispatch } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';

import { goUpdate } from '../../store/actions/profileAction';

import styles from './EditProfilStyles';

const EditProfilScreen = ({ route, navigation }) => {
  const profile = useSelector((state) => state.profileStore.profile);
  const isLoading = useSelector((state) => state.profileStore.isLoading);
  const [data, setData] = React.useState({
    username: null,
    password: null,
    email: null,
    phonenumber: [],
    check_textInputChange: false,
    isLoading: false,
    secureTextEntry: true,
    onError: false,
    onMessage: null,
    errorMsg: null,
    check_phonenumberChange: false,
    check_passwordChange: Boolean,
    check_NIKChange: false,
    check_KKChange: false
  });
  const dispatch = useDispatch();
  const [newNumber, setNewNumber] = useState('');
  const [newName, setNewName] = useState(null);
  const [newPassword, setNewPassword] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [show, setShow] = useState(false);
  const [onEditNumber, setOnEditNumber] = useState({ index: null, phone: '' });
  let [onSelected] = useState({});

  useEffect(() => {
    let params = route.params.data;
    setData({
      ...data,
      email: params.email,
      username: params.username,
      password: params.password,
      phonenumber: params.phonenumber
    });
  }, []);

  const updateProfile = async () => {
    let input = {
      name: !newName ? profile.name : newName,
      password: !newPassword ? profile.password : newPassword,
      phones: data.phonenumber
    };
    dispatch(goUpdate(input));
  };

  const usernameChange = (val) => {
    if (val.length !== 0 && val !== data.username) {
      setNewName(val);
    } else {
      setData({
        ...data,
        check_UsernameChange: false
      });
    }
  };

  const passwordChange = (val) => {
    if (val.length > 4 && val.length !== 0) {
      setData({
        ...data,
        password: val,
        check_passwordChange: true
      });
      setNewPassword(val);
    } else {
      setData({
        ...data,
        check_passwordChange: false
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      hidePsw: !data.hidePsw
    });
  };

  const changePhones = () => {
    let updatePhoneNumber = [];
    data.phonenumber.forEach((item, i) => {
      if (i === onEditNumber.index) { updatePhoneNumber.push({ phone: onEditNumber.phone }); }
      else { updatePhoneNumber.push(item); }
    });
    setData({ ...data, phonenumber: updatePhoneNumber });
    setModalVisible(false);
    setOnEditNumber({ index: null, phone: '' });
    setNewNumber('');
  };

  const deletePhones = () => {
    let updatePhoneNumber = [];
    data.phonenumber.forEach((item, i) => {
      if (i !== onEditNumber.index) { updatePhoneNumber.push(item); }
    });
    setData({ ...data, phonenumber: updatePhoneNumber });
    setModalVisible(false);
  };

  const addPhones = () => {
    setShow(false);
    if (data && data.phonenumber.length < 3) {
      setData({
        ...data,
        phonenumber: [
          ...data.phonenumber,
          { phone: newNumber.phone }
        ]
      });
    }
    else {
      Alert.alert('Maksimal 3 nomor');
    }
  };

  const validate_field = () => {
    let validate_phone = true;
    data.phonenumber.forEach(el => {
      let awal = el.phone?.substring(0,4);
      switch (awal) {
        case '0811':
        case '0812':
        case '0813':
        case '0852':
        case '0851':
        case '0821':
        case '0822':
        case '0823':
        case '0853':
          break;
        default:
          validate_phone = false;
          break;
      }
    });
    if (validate_phone === true){
      if (newName === '' || newName === null) {
        setNewName(data.username);
        return true;
      }
      else if (newPassword === '' || newPassword === null) {
        setNewPassword(data.password);
        return true;
      }
      return true;
    }
    else {
      Alert.alert('Masukkan Nomor Telkomsel');
    }
  };

  const register_success = () => {
    if (validate_field()) {
      updateProfile();
    }
  };


  return (
    <>
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}

        >

          <View style={{ backgroundColor: '#000000aa', flex: 1 }}>
            <View style={{ backgroundColor: '#ffffff', margin: width * 0.05, padding: width * 0.01, borderRadius: width * 0.05, marginTop: height * 0.4 }}>
              <Text style={styles.textPrivates}>Ubah Nomor Telepon Anda</Text>

              <TextInput
                placeholderTextColor="gray"
                style={styles.modal}
                placeholder="masukkan nomor telepon"
                value={onEditNumber.phone}
                onChangeText={text => setOnEditNumber({ ...onEditNumber, phone: text })}
                onSubmitEditing={changePhones}
              />
              <TouchableOpacity style={styles.modalbutton} onPress={changePhones}>
                <Text style={{ textAlign: 'center', fontSize: 15, padding: 8, fontWeight: 'bold', color: '#DE3030' }}>Save</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.modalButton} onPress={deletePhones}>
                <Text style={{ textAlign: 'center', fontSize: 13, padding: 8, fontWeight: 'bold', color: '#DE3030' }}>Delete</Text>
              </TouchableOpacity>
            </View>

          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          visible={show}
        >

          <View style={{ backgroundColor: '#000000aa', flex: 1 }}>
            <View style={{ backgroundColor: '#ffffff', margin: width * 0.03, padding: width * 0.05, borderRadius: width * 0.05, marginTop: height * 0.4 }}>
              <Text style={styles.textPrivates}>Ubah Nomor Telepon Anda</Text>

              <TextInput
                placeholderTextColor="gray"
                keyboardType="number-pad"
                placeholder="Masukkan nomor telepon"
                style={styles.modal}
                value={onSelected.phone}
                onChangeText={item => setNewNumber({ ...newNumber, phone: item })}
              />
              <TouchableOpacity style={styles.modalbutton} onPress={addPhones}>
                <Text style={{ textAlign: 'center', fontSize: 15, padding: 8, fontWeight: 'bold', color: '#DE3030' }}>Add</Text>
              </TouchableOpacity>
            </View>

          </View>
        </Modal>
      </View>
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.divider}>
            <Text style={styles.text_header}>Ubah Profil Anda</Text>
            <Image
              style={styles.pictureView}
              source={require('../../../assets/editprofile.png')} />
          </View>

        </View>

        <View style={styles.body}>
          <Animatable.View animation="fadeInUpBig">
            <View style={styles.action}>
              <TextInput
                placeholder="Ganti Nama Profile"
                placeholderTextColor="#DE3030"
                style={styles.textInput}
                onChangeText={(val) => usernameChange(val)}
              />
              <Feather name="users" color="#CD413A" size={20} style={styles.icon} />
            </View>
            <View style={styles.action}>
              <TextInput
                placeholder="Ganti Password"
                placeholderTextColor="#DE3030"
                secureTextEntry={!data.hidePsw}
                autoCapitalize="none"
                style={styles.textInput}
                onChangeText={(val) => passwordChange(val)}
              />
              <Feather name="key" color="#DE3030" size={20} style={styles.icon} />
              <TouchableOpacity disabled={data.isLoading} onPress={updateSecureTextEntry} style={styles.eye}>
                {data.hidePsw ?
                  <Feather name="eye" color="black" size={20} />
                  : <Feather name="eye-off" color="black" size={20} />
                }
              </TouchableOpacity>
              {data.check_passwordChange ?
                null
                : <Text style={styles.textError}>Password minimum contain 5 characters!</Text>}

            </View>

            <View style={styles.editphone}>

              <Text style={styles.textPrivate}>Ubah Nomor Handphone</Text>
              <TouchableOpacity style={styles.plus} onPress={() => { setShow(true); }}>
                <Feather name="plus" color="#DE3030" size={25} />
              </TouchableOpacity>
              <Text style={{ fontSize: 13, color: '#DE3030', paddingBottom: 10 }}>(klik untuk mengubah/tambah jika tidak ada)</Text>
              {data.phonenumber && data.phonenumber.map((item, i) => (
                <TouchableOpacity key={i} style={{ backgroundColor: 'white', alignItems: 'center' }}>
                  <Text style={styles.body_subtext} onPress={() => { setOnEditNumber({ index: i, phone: item.phone }); setModalVisible(true); }} >
                    Phone {i + 1} : {item.phone}</Text>
                </TouchableOpacity>
              ))}
            </View>


          </Animatable.View>
          <HelperText style={styles.textError} type="error" visible={data.onError}>
            {data.errorMsg ?
              data.errorMsg
              : 'Something Went Wrong!'}
          </HelperText>
        </View>

        <View style={styles.footer}>
          <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Text style={styles.textForgot}>Kembali </Text>
              <Feather name="arrow-left-circle" color="white" size={40} style={styles.icons} />
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
            <TouchableOpacity onPress={register_success}>
              <Text style={styles.textForgot}>Simpan Profil </Text>
              <Feather name="save" color="white" size={40} style={styles.icons} />
            </TouchableOpacity>
          </View>
        </View>
        <Spinner
          visible={isLoading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
          animation="fade"
        />
      </View>
    </>
  );
};

export default EditProfilScreen;
