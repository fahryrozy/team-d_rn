import { StyleSheet,Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    flex: 1,
    backgroundColor: '#DE3030'
  },
  divider: {
    flex: 1,
    alignSelf: 'center',
    top: height / 10
  },
  header: {
    flex: 1,
    height: height,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
    backgroundColor: '#DE3030'
  },
  body: {
    flex: 1,
    height: height,
    backgroundColor: '#DE3030',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  footer: {
    flex: 0.3,
    height: height,
    backgroundColor: '#DE3030',
    paddingHorizontal: 20,
    paddingVertical: height * 0.09
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    alignContent: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    fontSize: 30
  },
  textInput: {
    width: width * 0.9,
    height: height * 0.05,
    textAlign: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 20
  },
  icon: {
    position: 'absolute',
    left: width * 0.06,
    top: height * 0.015
  },
  icons: {
    position: 'absolute',
    alignSelf: 'flex-end',
    marginTop: width * 0.035
  },
  plus: {
    position: 'absolute',
    alignSelf: 'flex-end',
    marginTop: width * 0.02,
    right: width * 0.04
  },
  action: {
    marginTop: height * 0.02

  },
  editphone: {
    width: width * 0.9,
    alignSelf: 'center',
    borderRadius: 20,
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderColor: '#000',
    padding: 10,
    margin: 20
  },
  textPrivate: {
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#DE3030',
    alignContent: 'center',
    alignSelf: 'center'
  },
  textPrivates: {
    justifyContent: 'center',
    fontSize: 22,
    paddingTop: height * 0.01,
    fontWeight: 'bold',
    color: '#DE3030',
    alignContent: 'center',
    alignSelf: 'center'
  },
  pictureView: {
    flex: 1,
    position: 'absolute',
    alignSelf: 'center',
    marginTop: height * 0.05,
    width: 200,
    height: 200
  },
  eye: {
    position: 'absolute',
    left: width * 0.8,
    top: height * 0.015
  },
  body_subtext: {
    fontWeight: 'normal',
    fontSize: 15,
    color: '#DE3030',
    padding: height * 0.0005,
    fontFamily: 'arial',
    alignSelf: 'flex-start'
  },
  textError: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 10,
    alignSelf: 'center',
    fontFamily: 'arial',
    paddingTop: height * 0.015,
    marginLeft: 10
  },
  button: {
    alignItems: 'center',
    padding: height * 0.01
  },

  textForgot: {
    fontSize: 20,
    flexDirection: 'row',
    marginTop: width * 0.05,
    color: 'white',
    paddingBottom: 5,
    alignSelf: 'center',
    fontWeight: 'bold',
    marginRight: width * 0.1
  },


  color_textPrivate: {
    color: 'grey'
  },

  modalbutton: {
    width: width * 0.3,
    height: height * 0.04,
    backgroundColor: '#FCE5B9',
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: height * 0.02
  },
  modalButton: {
    width: width * 0.3,
    height: height * 0.04,
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: height * 0.01
  },
  modalButtons: {
    width: width * 0.3,
    height: height * 0.04,
    backgroundColor: 'grey',
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: height * 0.02
  },
  modal: {
    fontSize: 16,
    fontWeight: 'normal',
    color: 'grey',
    alignContent: 'center',
    textAlign: 'center',
    marginTop: height * 0.02
  }

});

export default styles;
