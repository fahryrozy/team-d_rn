import React, { useState, useRef, useEffect } from 'react';
import {
  Text,
  FlatList,
  View,
  TouchableOpacity,
  Dimensions,
  Alert
} from 'react-native';

import BottomSheet from 'reanimated-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useSelector, useDispatch } from 'react-redux';
import NumberFormat from 'react-number-format';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import LottieView from 'lottie-react-native';

import ButtonSheet from '../../components/ButtonBS';
import * as RootNavigation from '../../navigation/RootNavigation';
import { getOrders, getShipmentPrice } from '../../store/actions/orderAction';
import { checkoutOrder } from '../../store/actions/checkoutAction';
import CheckoutList from '../../components/CheckoutList';
import Address from '../../components/Address';
import { getListAddress } from '../../store/actions/addressAction';

import styles from './CheckoutStyle';

const { height } = Dimensions.get('window');

const CheckoutScreen = () => {
  const panelRef = useRef();
  const buttonRef = useRef();
  const addressRef = useRef();
  const dispatch = useDispatch();
  const [buttonTransparency, setButtonTransparency] = useState(false);
  const order = useSelector((state) => state.orderStore.order);
  const editAddress = useSelector((state) => state.addressStore.editAddress);
  const transaction = useSelector((state) => state.orderStore.current_transaction);
  const shipmentPrice = useSelector((state) => state.orderStore.shipmentPrice);
  const totalWeight = useSelector((state) => state.orderStore.weight);
  const totalPrice = useSelector((state) => state.orderStore.total_transaction);
  const address = useSelector((state) => state.addressStore.address);
  const defaultAddress = useSelector((state) => state.addressStore.defaultAddress);
  const selectedAddress = useSelector((state) => state.addressStore.selectedAddress);
  const deliveryAddress = Object.entries(selectedAddress).length >= 1 ? selectedAddress : defaultAddress;

  useEffect(() => {
    dispatch(getOrders());
    dispatch(getListAddress());
  }, [editAddress]);

  useEffect(()=> {
    addressRef.current.snapTo(0);
    dispatch(getShipmentPrice(selectedAddress.id, totalWeight));
  }, [selectedAddress]);

  const checkoutHandler = () => {
    if (deliveryAddress && Object.entries(deliveryAddress).length >= 1) {
      dispatch(checkoutOrder(transaction, deliveryAddress.id));
    }
    else {
      Alert.alert('Data Tidak Lengkap', 'Alamat pengiriman belum ada');
    }
  };

  const buttonSheet = () => {
    if (totalPrice > 0) {
      return (
        <NumberFormat value={totalPrice}
          displayType={'text'}
          prefix={'Rp. '}
          renderText={value => (
            <ButtonSheet
              style={buttonTransparency ? { opacity: 0.1 } : { opacity: 1 }}
              subtitle="Total Pembayaran"
              title={value} titleStyle={{ color: '#000' }}
              onPress={checkoutHandler} />
          )}
          thousandSeparator={'.'} decimalSeparator={','}
        />
      );
    }
    else {
      return <ButtonSheet title="Pesan" onPress={checkoutHandler} />;
    }
  };


  const addressSheet = () => (<Address data={address} onPress={()=>addressRef.current.snapTo(0)} />);

  const renderContent = () => (
    <Animatable.View animation="slideInUp" style={styles.panel}>
      <View style={styles.panelTitle}>
        <TouchableOpacity onPress={()=>RootNavigation.pop()}>
          <Ionicons name="chevron-back-circle" size={30} color={'#CD413A'} style={{ paddingRight: 10, opacity: 0.3 }} />
        </TouchableOpacity>
        <Text style={styles.title}>Checkout</Text>
      </View>

      <View style={styles.panelAddress}>
        <View style={styles.labelContainer}>
          <Text style={styles.label}>Alamat Pengiriman</Text>
          {address.length >= 1 ?
            (
              <TouchableOpacity onPress={()=>addressRef.current.snapTo(1)}>
                <FontAweSome name="edit" size={30} color={'#F18F40'} style={{ paddingHorizontal: 10, opacity: 0.5 }} />
              </TouchableOpacity>

            )
            : (
              <TouchableOpacity onPress={()=>RootNavigation.navigate('Address')}>
                <FontAweSome name="edit" size={30} color={'#F18F40'} style={{ paddingHorizontal: 10, opacity: 0.5 }} />
              </TouchableOpacity>
            )
          }
        </View>
        {address.length >= 1 ?
          (
            <>
              <Text style={styles.addressTitle}>{deliveryAddress.addressName}</Text>
              <Text style={styles.address}>
                {deliveryAddress.street}, {deliveryAddress.district}, {deliveryAddress.regency}, {deliveryAddress.state}, {deliveryAddress.postalCode}
              </Text>
            </>
          )
          : (
            <Text style={styles.addressTitle}>Silahkan masukkan alamat anda</Text>
          )
        }
      </View>
      <View style={styles.deliveryFees}>
        <Text style={styles.label}>Biaya Pengiriman</Text>
        {shipmentPrice > 0 ? <NumberFormat value={shipmentPrice}
          displayType={'text'}
          prefix={'Rp. '}
          renderText={value => (
            <Text style={styles.addressTitle}>{value}</Text>
          )}
          thousandSeparator={'.'} decimalSeparator={','}
        />
          : <Text style={styles.addressTitle}>Silahkan pilih alamat terlebih dahulu</Text>
        }
      </View>

      <View style={styles.panelPayment}>
        <Text style={styles.label}>Total : {order.totalItem} Items</Text>
        <FlatList data={order.product}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => <CheckoutList data={item} />}
          showsVerticalScrollIndicator={false}
          onScrollBeginDrag={() => setButtonTransparency(true)}
          onScrollEndDrag={() => setButtonTransparency(false)}
        />
      </View>
    </Animatable.View>
  );

  const renderHeader = () => (
    <Animatable.View animation="slideInUp" style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </Animatable.View>
  );

  return (
    <>
      <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
        <LottieView source={require('./../../asset/lottiefiles/shipping-label-printing.json')} autoPlay loop />
      </LinearGradient>

      <BottomSheet
        ref={panelRef}
        snapPoints={[height * 0.9, height * 0.5, height * 0.2]}
        renderContent={renderContent}
        renderHeader={renderHeader}
        initialSnap={0}
        enabledInnerScrolling={true}
        enabledContentGestureInteraction={false}
      />

      <BottomSheet
        ref={buttonRef}
        snapPoints={[height * 0.15]}
        renderContent={buttonSheet}
        initialSnap={0}
      />

      <BottomSheet
        ref={addressRef}
        snapPoints={[0, height * 0.925]}
        renderContent={addressSheet}
        renderHeader={renderHeader}
        initialSnap={0}
        enabledInnerScrolling={true}
        enabledContentGestureInteraction={false}
      />
    </>
  );
};

export default CheckoutScreen;
