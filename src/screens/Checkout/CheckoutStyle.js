import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CD413A',
    alignItems: 'center'
  },
  panel: {
    marginTop: -10,
    height: height * 0.925,
    paddingVertical: 20,
    paddingBottom: height * 0.1,
    paddingHorizontal: 20,
    backgroundColor: '#FEFAF5'
  },
  header: {
    backgroundColor: '#FEFAF5',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 100,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#F18F40',
    marginBottom: 5
  },
  panelTitle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    fontSize: width * 0.075,
    fontWeight: 'bold',
    color: '#CD413A',
    width: '75%',
    textAlign: 'center'
  },
  panelAddress: {
    flex: 2,
    paddingVertical: 10
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  label: {
    fontSize: width * 0.05,
    fontWeight: 'bold',
    color: '#F18F40',
    marginVertical: 5
  },
  addressTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#262626'
  },
  address: {
    fontWeight: '500',
    fontSize: 13,
    color: '#262626'
  },
  deliveryFees: {
    flex: 1,
    paddingVertical: 20
  },
  panelPayment: {
    flex: 9,
    paddingVertical: 20
  }
});
export default styles;
