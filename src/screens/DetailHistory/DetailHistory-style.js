import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#De3030'
  },
  containerTransHis: {
    width: 0.786 * width,
    height: 0.18 * height,
    borderColor: '#F18F40',
    borderRadius: 20,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
    backgroundColor: 'white',
    alignSelf: 'center'


  },
  imgcard: {
    width: 50,
    height: 50
  },
  fontID: {
    color: '#F18F40',
    fontWeight: 'bold',
    fontSize: 10
  },
  fontName: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 14,
    width: 0.5545 * width
  },
  fontStatus: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 15
  },
  fontStatus2: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 15
  },
  status: {
    backgroundColor: '#60CD3A',
    width: 0.21545 * width,
    borderRadius: 20,
    alignItems: 'center',
    position: 'absolute',
    marginTop: 0.085 * height,
    marginLeft: 0.37 * width
  },
  statusWaiting: {
    backgroundColor: '#CD413A',
    width: 0.351545 * width,
    borderRadius: 20,
    alignItems: 'center',
    position: 'absolute',
    marginLeft: 0.24 * width,
    marginTop: 0.0802 * height
  },
  fontOthers: {
    color: 'grey',
    fontSize: 12
  },
  fontTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 10
  },
  containerKanan: {
    marginLeft: 10,
    height: 0.125 * height,
    width: 0.5745 * width
  },
  fontTotal: {
    color: 'black',
    fontSize: 12
  },
  GantiHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 0.0555 * width,
    marginRight: 0.0555 * width,
    marginTop: 0.0312 * height,
    paddingBottom: 15
  },
  fontGanti: {
    fontSize: 20,
    color: 'white',
    alignSelf: 'center',
    marginLeft: 0.09420 * width,
    fontWeight: 'bold'
  },
  containerProduk: {
    backgroundColor: '#fffaf4'

  },
  fontJudul: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold'
  },
  containerStatus: {
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderTopColor: 'white',
    backgroundColor: 'white',
    borderRadius: 25
  },
  containerPengiriman: {
    paddingBottom: 15,
    paddingTop: 15,
    borderBottomColor: 'white',
    borderTopWidth: 1,
    borderTopColor: 'white'
  },
  containerPembayaran: {
    paddingBottom: 15,
    paddingTop: 15,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    borderTopWidth: 1,
    borderTopColor: 'white',
    paddingLeft: 15,
    paddingRight: 15
  },
  containerPayment: {
    backgroundColor: 'white',
    borderRadius: 25,
    padding: 8,
    marginTop: 10
  },
  containerAlamat: {
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderRadius: 25,
    padding: 8,
    marginTop: 10
  },
  fontStatusItem: {
    fontSize: 16,
    color: 'black',
    marginBottom: 5
  },
  fontAlamat: {
    fontSize: 15,
    color: 'black'

  },
  alamatKiri: {
    flex: 0.7
  },
  review: {
    backgroundColor: '#60CD3A',
    width: 0.21545 * width,
    borderRadius: 20,
    alignItems: 'center',
    position: 'absolute',
    marginTop: 0.008 * height,
    marginLeft: 0.35 * width
  },
  panel: {
    height: height * 0.9,
    paddingVertical: 20,
    paddingHorizontal: 30,
    backgroundColor: '#FEFAF5'
  },
  inputReview: {
    backgroundColor: '#EEECEC',
    borderRadius: 10,
    width: 0.75 * width,
    height: 0.1578125 * height,
    alignSelf: 'center',
    marginTop: 0.015625 * height,
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
    textAlign: 'left',
    textAlignVertical: 'top'
  },
  containerModal: {
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderRadius: 20,
    height: 0.50625 * height,
    width: 0.9055555 * width,
    marginBottom: 0.2640625 * height
  },
  textReview: {
    fontSize: 16,
    alignSelf: 'center',
    fontFamily: 'roboto',
    color: '#000000',
    marginTop: 0.0328125 * height
  },
  textRating: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#000000',
    marginTop: 0.021875 * height
  },
  inputDesc: {
    backgroundColor: '#EEECEC',
    borderRadius: 10,
    width: 0.75 * width,
    height: 0.1578125 * height,
    alignSelf: 'center',
    marginTop: 0.015625 * height,
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
    textAlign: 'left',
    textAlignVertical: 'top'
  },
  buttonCancel: {
    height: 0.046875 * height,
    width: 0.269444 * width,
    backgroundColor: 'gray',
    borderRadius: 100,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 0.05625 * height
  },
  textCancel: {
    color: '#111500',
    alignSelf: 'center',
    justifyContent: 'center',
    fontWeight: '700'
  },
  buttonSubmit: {
    height: 0.046875 * height,
    width: 0.269444 * width,
    backgroundColor: '#60CD3A',
    borderRadius: 100,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 0.05625 * height
  },
  textSubmit: {
    color: '#111500',
    alignSelf: 'center',
    justifyContent: 'center',
    fontWeight: '700'
  }




});
export default styles;
