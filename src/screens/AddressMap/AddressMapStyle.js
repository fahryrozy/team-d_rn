import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DE3030',
    alignItems: 'center'
  },
  searchBar: {
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 1
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  buttonContainer: {
    height: height * 0.1
  },
  buttonSave: {
    backgroundColor: '#DE3030',
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    justifyContent: 'center'
  },
  buttonTitle: {
    color: '#FFF',
    fontSize: width * 0.04
  },
  textInput: { width: width, backgroundColor: '#FFF', justifyContent: 'center', alignItems: 'center' }
});
export default styles;
