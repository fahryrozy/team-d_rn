import React, { useState, useEffect, useRef } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Dimensions } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import MapView, { Marker } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useSelector, useDispatch } from 'react-redux';
import Config from 'react-native-config';

import * as RootNavigation from '../../navigation/RootNavigation';
import { setLongLat } from '../../store/actions/longlatAction';
import { hasLocationPermission, requestPermissions } from '../../utils/location';

import styles from './AddressMapStyle';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

const AddressMapScreen = () => {
  const ref = useRef();
  const dispatch = useDispatch();

  const { GOOGLE_MAP_API_KEY } = Config;

  const myLong = useSelector((state) => state.addressStore.mylong);
  const myLat = useSelector((state) => state.addressStore.mylat);

  const [myLocationLatitude, setMyLocationLatitude] = useState();
  const [myLocationLongitude, setMyLocationLongitude] = useState();
  const [markerLatitude, setMarkerLatitude] = useState();
  const [markerLongitude, setMarkerLongitude] = useState();

  const LATITUDE = markerLatitude ? markerLatitude : myLat;
  const LONGITUDE = markerLongitude ? markerLongitude : myLong;
  const LATITUDE_DELTA = 0.0922;
  const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

  const [initialRegion, setInitialRegion] = useState({
    latitude: LATITUDE,
    longitude: LONGITUDE,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA
  });

  const homePlace = {
    description: 'My Current Location',
    geometry: { location: { lat: myLocationLatitude, lng: myLocationLongitude } }
  };

  useEffect(()=>{
    hasLocationPermission();
    requestPermissions();
    getMyLocation();
  }, []);

  const getMyLocation = () => Geolocation.getCurrentPosition(
    (position) => {
      setMyLocationLatitude(position.coords.latitude);
      setMyLocationLongitude(position.coords.longitude);
      setMarkerLatitude(position.coords.latitude);
      setMarkerLongitude(position.coords.longitude);
    }
  );

  const dragMarker = (e) => {
    setMarkerLatitude(e.nativeEvent.coordinate.latitude);
    setMarkerLongitude(e.nativeEvent.coordinate.longitude);
  };

  const regionChange = (region) => {
    setInitialRegion(region);
    setMarkerLatitude(region.latitude);
    setMarkerLongitude(region.longitude);
  };

  const selectAutoComplete = (data, details) => {
    setMarkerLatitude(details.geometry.location.lat);
    setMarkerLongitude(details.geometry.location.lng);

    setInitialRegion({
      latitude: details.geometry.location.lat,
      longitude: details.geometry.location.lng,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA
    });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <MapView style={styles.map} region={initialRegion} onRegionChangeComplete={regionChange} >
        {markerLatitude && markerLongitude &&
          <Marker draggable coordinate={{ longitude: markerLongitude, latitude: markerLatitude }} onDragEnd={dragMarker} />
        }
      </MapView>

      <GooglePlacesAutocomplete ref={ref} styles={{ textInputContainer: styles.textInput }}
        renderLeftButton={()=>(
          <TouchableOpacity onPress={()=>RootNavigation.pop()}>
            <Ionicons name="chevron-back-circle" size={30} color={'#DE3030'} style={{ paddingHorizontal: 10, opacity: 0.5 }} />
          </TouchableOpacity>
        )
        }
        renderRightButton={()=>(
          <TouchableOpacity onPress={()=>ref.current?.clear()}>
            <MaterialIcons name="clear" size={30} color={'#DE3030'} style={{ paddingHorizontal: 10, opacity: 0.5 }} />
          </TouchableOpacity>
        )
        }
        placeholder="Search" fetchDetails={true}
        onPress={selectAutoComplete}
        query={{
          key: GOOGLE_MAP_API_KEY,
          language: 'en',
          components: 'country:id'
        }}
        enablePoweredByContainer={false}
        nearbyPlacesAPI="GoogleReverseGeocoding" predefinedPlaces={[homePlace]}
      />

      {markerLatitude && markerLongitude &&
        <TouchableOpacity style={styles.buttonContainer} onPress={()=>dispatch(setLongLat(markerLongitude, markerLatitude))}>
          <View style={styles.buttonSave}>
            <Text style={styles.buttonTitle}>Pilih Lokasi</Text>
          </View>
        </TouchableOpacity>
      }
    </SafeAreaView>
  );
};

export default AddressMapScreen;
