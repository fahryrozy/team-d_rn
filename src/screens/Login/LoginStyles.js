import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    flex: 1
  },
  divider: {
    flex: 1,
    alignSelf: 'center',
    top: height / 10
  },
  header: {
    flex: 1,
    height: height * 0.5,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50
  },
  pictureView: {
    width: width,
    height: width
  },
  eye: {
    position: 'absolute',
    left: width * 0.8,
    top: height * 0.015
  },
  body: {
    flex: 1,
    height: height * 0.2,
    top: height * 0.05,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  footer: {
    flex: 1,
    height: height * 0.2,
    paddingHorizontal: width * 0.05,
    paddingVertical: width * 0.08
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    alignContent: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    fontSize: 30
  },
  textInput: {
    width: width * 0.9,
    height: height * 0.05,
    textAlign: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 20
  },
  icon: {
    position: 'absolute',
    left: width * 0.06,
    top: height * 0.015
  },
  icons: {
    position: 'absolute',
    alignSelf: 'flex-end'
  },
  action: {
    marginTop: height * 0.02

  },
  textPrivate: {
    fontSize: 30,
    fontWeight: 'bold',
    color: 'white',
    marginRight: width * 0.1
  },
  text: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'white',
    marginTop: height * 0.02
  },
  textError: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 10,
    alignSelf: 'center',
    fontFamily: 'arial',
    paddingTop: height * 0.015,
    marginLeft: 10
  }

});

export default styles;
