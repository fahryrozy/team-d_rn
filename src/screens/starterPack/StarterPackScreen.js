import React from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { CartIcon, BackIcon } from '../../components/Icon';
import TabViewExample from '../../components/TabViewStarterPack';
import * as RootNavigation from '../../navigation/RootNavigation';

import styles from './StarterPackStyles';

export default function StarterPackScreen() {
  return (
    <>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#e52d27', '#b31217']}
        style={styles.linearGradient}>
        <View>
          <View style={styles.GantiHeader}>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity onPress={() => RootNavigation.navigate('Home')}>
                <BackIcon />
              </TouchableOpacity>
              <Text style={styles.fontGanti}>Starter Pack</Text>
            </View>
            <View>
              <TouchableOpacity onPress={() => RootNavigation.navigate('Cart')}>
                <CartIcon />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </LinearGradient>
      <TabViewExample />
    </>
  );
}
