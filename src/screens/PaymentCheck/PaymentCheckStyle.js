import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DE3030',
    alignItems: 'center'
  },
  titleBar: {
    marginTop: 50,
    alignItems: 'center',
    width: width,
    height: height * 0.5
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFF'
  },
  label: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#DE3030'
  },
  cartImage: {
    width: 1 * width,
    height: 0.5 * height,
    alignSelf: 'center',
    marginBottom: 10
  },
  panel: {
    height: height * 0.7,
    paddingVertical: 20,
    paddingHorizontal: 20,
    backgroundColor: '#FEFAF5'
  },
  header: {
    backgroundColor: '#FEFAF5',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 100,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#F18F40',
    marginBottom: 5
  },
  panelTitle: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  panelPhoto: {
    width: '100%',
    backgroundColor: '#FFF',
    height: 200,
    marginTop: height * 0.04,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
export default styles;
