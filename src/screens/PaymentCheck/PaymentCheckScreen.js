import React, { useState, useRef } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
  Alert,
  ToastAndroid,
  Dimensions } from 'react-native';

import BottomSheet from 'reanimated-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { useSelector, useDispatch } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import LottieView from 'lottie-react-native';

import { requestCameraPermission, requestExternalWritePermission } from '../../utils/Permission';
import * as RootNavigation from '../../navigation/RootNavigation';
import ButtonSheet from '../../components/ButtonBS';
import { ImagePickerBSContent, ImagePickerBSHeader } from '../../components/ImagePickerBS';
import { confirm_payment } from '../../store/actions/paymentAction';

import styles from './PaymentCheckStyle';

const { height } = Dimensions.get('window');

const PaymentCheckScreen = () => {
  const [imageFile, setImageFile] = useState();
  const [imageBase64, setImageBase64] = useState();

  const panelRef = useRef();
  const buttonRef = useRef();
  const pickImageRef = useRef();

  const dispatch = useDispatch();
  const transaction = useSelector((state) => state.orderStore.current_transaction);
  const paymentMethodId = useSelector((state) => state.bankStore.selectedBank);
  const totalPrice = useSelector((state) => state.orderStore.total_transaction);

  const paymentCheckHandler = () => {
    if (imageBase64) {
      dispatch(confirm_payment(transaction, paymentMethodId, totalPrice, imageBase64));
    }
    else {
      ToastAndroid.showWithGravity('Masukkan bukti transfer terlebih dahulu', ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };

  const fileOptions = {
    mediaType: 'photo',
    maxWidth: 1000,
    maxHeight: 1000,
    includeBase64: true,
    quality: 1
  };

  const chooseFile = () => {
    launchImageLibrary(fileOptions, (response) => {
      setImageBase64(response.base64);
      setImageFile(response.uri);
      pickImageRef.current.snapTo(1);
    });
  };

  const captureImage = async () => {
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      launchCamera(fileOptions, (response) => {
        if (response.errorCode === 'camera_unavailable') {
          Alert.alert('Camera not available on device');
          return;
        } else if (response.errorCode === 'permission') {
          Alert.lert('Permission not satisfied');
          return;
        } else if (response.errorCode === 'others') {
          Alert.alert(response.errorMessage);
          return;
        }
        setImageBase64(response.base64);
        setImageFile(response.uri);
        pickImageRef.current.snapTo(1);
      });
    }
  };

  const ImagePickHeader = () => (<ImagePickerBSHeader />);
  const ImagePickContent = () => (
    <ImagePickerBSContent
      takePhotoAction={captureImage}
      chooseImageAction={chooseFile}
      cancelAction={()=>pickImageRef.current.snapTo(1)}
    />
  );
  const buttonSheet = () => (<ButtonSheet title="Upload" onPress={paymentCheckHandler} />);

  const renderContent = () => (
    <Animatable.View animation="slideInUp" style={styles.panel}>
      <View style={styles.panelTitle}>
        <TouchableOpacity onPress={()=>RootNavigation.pop()}>
          <Ionicons name="chevron-back-circle" size={30} color={'#DE3030'} style={{ paddingRight: 10, opacity: 0.3 }} />
        </TouchableOpacity>

        <Text style={styles.label}>Upload bukti transfer</Text>
      </View>

      {imageFile ?
        (
          <ImageBackground source={{ uri: imageFile }} style={styles.panelPhoto} >
            <TouchableOpacity onPress={()=>{pickImageRef.current.snapTo(0);}}>
              <Ionicons name="camera" size={100} color={'#AAA'} />
            </TouchableOpacity>
          </ImageBackground>
        )
        : (
          <View style={styles.panelPhoto}>
            <TouchableOpacity onPress={()=>{pickImageRef.current.snapTo(0);}}>
              <Ionicons name="camera" size={100} color={'#AAA'} />
            </TouchableOpacity>
          </View>
        )
      }
    </Animatable.View>
  );

  const renderHeader = () => (
    <Animatable.View animation="slideInUp" style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </Animatable.View>
  );

  return (
    <>
      <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
        <View style={styles.titleBar}>
          <Text style={styles.title}>Anda Hampir Selesai</Text>
          <LottieView source={require('./../../asset/lottiefiles/make-payment.json')} autoPlay loop style={{ marginTop: -40 }} />
        </View>
      </LinearGradient>

      <BottomSheet
        ref={panelRef}
        snapPoints={[height * 0.6, height * 0.5]}
        renderContent={renderContent}
        renderHeader={renderHeader}
        initialSnap={0}
      />

      <BottomSheet
        ref={buttonRef}
        snapPoints={[height * 0.15]}
        renderContent={buttonSheet}
        initialSnap={0}
      />

      <BottomSheet
        ref={pickImageRef}
        snapPoints={[280, 0]}
        renderContent={ImagePickContent}
        renderHeader={ImagePickHeader}
        initialSnap={1}
        enabledHeaderGestureInteraction={true}
      />
    </>
  );
};

export default PaymentCheckScreen;
