import React from 'react';

import VirtualPaymentScreen from './VirtualPaymentScreen';

const VirtualPayment = () => {
  return (
    <VirtualPaymentScreen />
  );
};

export default VirtualPayment;
