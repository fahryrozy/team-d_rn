import React, { useEffect } from 'react';
import {
  Text,
  View,
  TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import LottieView from 'lottie-react-native';
import LinearGradient from 'react-native-linear-gradient';

import * as RootNavigation from '../../navigation/RootNavigation';
import { getOrders } from '../../store/actions/orderAction';

import styles from './VirtualPaymentStyle';

const VirtualPaymentScreen = () => {
  const dispatch = useDispatch();
  const bank = useSelector((state) => state.paymentStore.bank);
  const vaNumber = useSelector((state) => state.paymentStore.vaNumber);

  useEffect(() => {
    dispatch(getOrders());
  }, []);

  return (
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
      <View style={styles.titleBar}>
        <Text style={styles.title}>Virtual Account {bank && bank.toUpperCase()}</Text>
        <Text style={styles.virtualAccountNumber}>{vaNumber}</Text>
      </View>
      <LottieView source={require('../../asset/lottiefiles/financial-transactions.json')} autoPlay loop />
      <Text style={styles.subtitle}>Kami akan check pembayaran kamu secara otomatis </Text>
      <TouchableOpacity onPress={()=>RootNavigation.popToTop()}>
        <View style={styles.buttonContainer}>
          <Text style={styles.buttonTitle}>Kembali ke Home</Text>
        </View>
      </TouchableOpacity>
    </LinearGradient>
  );
};

export default VirtualPaymentScreen;
