export const carouselData =
[{
  title: 'Replace SIM Card', url: require('../../assets/onboarding-img1.png'),
  description: 'Ga perlu repot keluar rumah, pesan sekarang, kami segera mengirimkan SIM baru ke rumah mu.',
  id: 1

},
{
  title: 'Starter Pack Online', url: require('../../assets/onboarding-img2.png'),
  description: 'Kami ada banyak penawaran menarik untuk kamu. Cek sekarang yuk!',
  id: 2
},
{
  title: 'Device Bundling', url: require('../../assets/onboarding-img3.png'),
  description: 'Beli HP dapat kuota murah?, kapan lagi hanya disini kamu bisa dapetin.',
  id: 3
},
{
  title: 'Get Postpaid Number', url: require('../../assets/Picture1.png'),
  description: 'Buat yang gak mau repot isi pulsa. Beli HALO sekarang, banyak penawaran menarik.',
  id: 4
}
];
